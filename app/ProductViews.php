<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductViews extends Model
{

    function products()
    {
        return $this->hasMany('App\Models\Product', 'parent_id', 'parent_id');
    }
    //ENDS
}
