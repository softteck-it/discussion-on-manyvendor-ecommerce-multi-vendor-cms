<?php

namespace App\Http\Controllers\backend\admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ImpersonatorController extends Controller
{
    
    public function impersonate(Request $request, $user_id)
    {
        # Check if user is logged in
        if (!Auth::check()) {
            abort(403);
        }
        $current = Auth::user();

        # Check if current user is Admin
        # Example from Spatie Laravel Permission to check if user has admin role
        # Adjust this code with your package
        if (!$current->user_type === 'Admin') {
            abort(403);
        }

        $user = User::findOrFail($user_id);

        $meta = array(
            'user_id'  => $current->id,
            'back_url'  => url()->previous(),
            'target_user' => $user->id
        );

        Session::put('impersonation', json_encode($meta));

        ## Redirect to User Dashboard
        return redirect('/');
    }

    public function rollback()
    {
        # Check if user is logged in
        if (!Auth::check()) {
            abort(403);
        }
        if (Session::has('impersonation')) {
            $s = json_decode(Session::get('impersonation'));
            if (json_last_error() === JSON_ERROR_NONE) {
                if (isset($s->user_id)) {
                    Session::forget('impersonation');
                    Auth::loginUsingId($s->user_id);

                    # Redirect to back url
                    return redirect()->to($s->back_url);
                }
            }
        }

        return redirect('/');
    }
}
