<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\SendEmailMarketing;
use Alert;

class MarketingController extends Controller
{

    /**
     * email_marketing
     */
    public function email_marketing(){
        return view('marketing.email.index');
    }
    /**
     * email_marketing
     */
    public function send_email(Request $request){

        $this->validate($request, [
            'sender_email' => 'required|email',
            'subject' => 'required',
            'sender_name' => 'required',
            'template' => 'required',
        ]);

            $details = [
                'subject' => $request->subject,
                'sender_email' => $request->sender_email,
                'sender_name' => $request->sender_name,
                'template' => $request->template
            ];
      
        $emails = $request->emails;

        try {
            foreach ($emails as $email) {
                // php artisan queue:work --queue=high,default
                \Mail::to([$email])->queue(new SendEmailMarketing($details));
            }
        } catch (\Throwable $th) {
            Alert::toast(translate('Please configure the SMTP'), 'error');
            return back();
        }

        Alert::toast(translate('Email queue is running'), 'success');
        return back();
    }

    /**
     * sms_marketing
     */
    public function sms_marketing(){
        return view('marketing.sms.index');
    }
    //END
}
