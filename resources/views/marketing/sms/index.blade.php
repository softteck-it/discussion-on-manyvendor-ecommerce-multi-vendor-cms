@extends('backend.layouts.master')
@section('title') @translate(SMS Marketing) @endsection
@section('content')

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title"> @translate(SMS Marketing)</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body p-2">


            <!-- Content starts here -->
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card-body">
                        <form method="post" action="{{route('logistics.area.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div>

                                <div class="form-group">
                                    <label>@translate(Phone Numbers(users))</label>
                                    <select class="form-control select2 division" name="division_id" required>
                                        <option value="">@translate(Select Division)</option>
                                        <option value="">@translate(Select Division)</option>
                                        <option value="">@translate(Select Division)</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>@translate(Select Area)</label>
                                    <select class="form-control select2 area" name="area_id[]" multiple required></select>
                                </div>

                                <button type="submit" class="btn btn-primary">@translate(Save)</button>

                            </div>

                        </form>
                    </div>
                </div>

            </div>

            <!-- Content starts here:END -->

        </div>

    </div>
 

@endsection
