@extends('backend.layouts.master')
@section('title') @translate(Email Marketing) @endsection
@section('css') 
<link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
@endsection
@section('content')

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">@translate(Email Marketing)</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body p-2">


            <!-- Content starts here -->
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="card-body">
                        <form method="get" action="{{route('marketing.send.email')}}" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label>@translate(Emails(users))</label>
                                   
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                            <select class="duallistbox" multiple="multiple" name="emails[]">
                                                @foreach (App\User::get() as $email)
                                                    <option value="{{ $email->email }}" class="p-2" {{ (collect(old('emails'))->contains($email->email)) ? 'data-sortindex="'.$email->id.'"':'' }}>{{ $email->email }}</option>
                                                @endforeach
                                            </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                        <!-- /.row -->
                                   
                                </div>

                                 <div class="form-group">
                                    <label>@translate(Newsletter subject)</label>
                                    <input type="text" class="form-control" name="subject" placeholder="@translate(Newsletter subject)" required value="{{ old('subject') }}">
                                </div>

                                <div class="form-group">
                                    <label>@translate(Sender Email Address)</label>
                                    <input type="email" class="form-control" name="sender_email" placeholder="@translate(Sender Email Address)"  required value="{{ old('sender_email') }}">
                                </div>

                                <div class="form-group">
                                    <label>@translate(Sender Name)</label>
                                    <input type="text" class="form-control" name="sender_name" placeholder="@translate(Sender Name)" required value="{{ old('sender_name') }}">
                                </div>

                                <div class="form-group">
                                    <label>@translate(Email Temaples)</label>
                                    <select class="form-control" name="template" required>
                                        <option value="" selected>@translate(Select Template)</option>
                                        @foreach(mailEclipse::getTemplates() as $template)
                                            <option value="{{ $template->template_slug }}">{{ ucwords($template->template_name) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary">@translate(Send Now)</button>

                        </form>
                    </div>
                </div>

            </div>

            <!-- Content starts here:END -->

        </div>
    </div>

@endsection

@section('script')
<script src="https://adminlte.io/themes/v3/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>   
<script>
    $(".duallistbox").bootstrapDualListbox({
    selectorMinimalHeight: 300,
});
</script> 
@endsection
