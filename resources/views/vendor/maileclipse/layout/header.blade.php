<header>
        <nav class="navbar navbar-dark bg-dark header">
            <div class="container">
                <div class="d-flex align-items-center header">
                    <a href="{{route('home')}}" class="d-flex justify-content-center">
                        <img src="{{filePath(getSystemSetting('type_logo'))}}" alt="{{getSystemSetting('type_name')}}"
                            class="img-fluid aside-logo">
                    </a>
                </div>
            </div>
        </nav>
    </header>