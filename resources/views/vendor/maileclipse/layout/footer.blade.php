<footer class="text-right mt-4" style="font-size: 14px;">
      <div class="text-muted">
          <strong>@translate(Copyright) &copy; {{date('Y')}} {{getSystemSetting('type_footer')}} {{ env('APP_VERSION') }}</strong>
      </div>      
      
 </footer>
