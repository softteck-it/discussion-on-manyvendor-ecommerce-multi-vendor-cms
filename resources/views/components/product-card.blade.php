<div class="product-container">
    <a href="{{ $href }}">
        <div class="product-image-container" style="padding-bottom: 100%;">
            <span class="sale-badge d-none">sale</span>
            <img class="product-image" src="{{ $image }}" alt="{{ $name }}">
        </div>

        <div class="product-dscr-container">
            <div class="product-title">
                {{ $name }}
            </div>
            <div class="product-price">

                @if (vendorActive())
                    {{ $product_price }}
                @else
                    @if ($is_discount == '1')
                        {{ $discount_price }}
                        <del>
                            {{ $product_price }}
                        </del>
                    @else
                        {{ $product_price }}
                    @endif

                @endif

            </div>
            {{-- <div class="oriArea">
                US $19.99
                <div class="discount-block">-20%</div>
            </div> --}}
            <div class="progress mt-2 mb-2 bg-danger">
                <div class="progress-bar bg-{{ product_stock_percentage($product_id) <= 30 ? 'danger' : 'warning' }}"
                    role="progressbar" style="width: {{ product_stock_percentage($product_id) }}%;"
                    aria-valuenow="{{ product_stock_percentage($product_id) }}" aria-valuemin="0" aria-valuemax="100">
                    {{ total_left_product_stock($product_id) }} @translate(left)</div>
            </div>
            <div class="sold-area">
                @isset($product_brand)
                    <div class="star-icon" style="background: url({{ asset($brand_logo) }}) no-repeat;">
                    </div>{{ $product_id }} {{ Str::limit($product_brand, 13) }} |
                @endisset
                <div class="product-sold-count">{{ total_sold_product($product_id) }} @translate(Sold)</div>
            </div>
        </div>
    </a>
</div>
