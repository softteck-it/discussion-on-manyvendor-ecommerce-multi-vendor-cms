<div class="product-container">
    <a href="{{ $shop_href }}">
        <div class="product-image-container" style="padding-bottom: 100%;">
                <img class="product-image" src="{{ $shop_logo }}" alt="{{ $shop_name }}">
        </div>
    
        <div class="product-dscr-container">
            <div class="product-title text-center">
                {{ $shop_name }}
            </div>
        </div>
    </a>
</div>