<div class="product-container">
    <a href="{{ $brand_href }}">
        <div class="product-image-container" style="padding-bottom: 100%;">
                <img class="product-image" src="{{ $brand_logo }}" alt="{{ $brand_name }}">
        </div>
    
        <div class="product-dscr-container">
            <div class="product-title text-center">
                {{ $brand_name }}
            </div>
        </div>
    </a>
</div>