@extends('frontend.master')


@section('title')

@section('content')

    <!-- breadcrumb:START -->
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('homepage') }}">@translate(Home)</a></li>
                <li>@translate(Brands)</li>
            </ul>
        </div>
    </div>
    <!-- breadcrumb:END -->

    <!-- SHOP LIST:START -->
    <div class="ps-whishlist">
        <div class="container">
            <div class="ps-section__header my-container text-center text-white m-4">
                <h1>@translate(All Brands)</h1>
            </div>
            <div class="ps-section__content">
                <!-- all brand list -->
                <div class="container">
                    <form class="ps-form--newsletter" action="" method="get">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <div class="ps-form__right">
                                    <div class="form-group--nest">
                                        <input class="form-control" name="search" value="{{Request::get('search')}}" type="text" id="myInput"
                                               placeholder="@translate(Search brand here)">
                                        <button class="ps-btn" type="submit">@translate(Search)</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="container" id="myShop">
                    <div class="row mt-5 no-gutters">
                        <!-- HERE GOES SHOPS -->
                        
                                @forelse ($brands as $brand)
                                    <div class="col-md-2 col-xl-2">
                                        <x-brand-card>
                                            <x-slot name="brand_id">{{ $brand->id }}</x-slot>
                                            <x-slot name="brand_slug">{{ $brand->slug }}</x-slot>
                                            <x-slot name="brand_name">{{ $brand->name }}</x-slot>
                                            <x-slot name="brand_logo">
                                                @if (empty($brand->logo))
                                                    {{asset('vendor-store.jpg')}}
                                                @else
                                                    {{ filePath($brand->logo) }}
                                                @endif
                                            </x-slot>
                                            <x-slot name="brand_href">{{ route('brand.shop', $brand->slug) }}</x-slot>
                                        </x-brand-card>
                                    </div>
                              @empty
                                <img src="{{ asset('No-Brand-Found.jpg') }}" class="img-fluid" alt="#no-brand-found">
                              @endforelse
                              
                              
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- SHOP LIST:END -->


@stop

