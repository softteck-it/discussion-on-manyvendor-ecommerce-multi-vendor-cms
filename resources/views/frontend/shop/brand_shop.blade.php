@extends('frontend.master')

@section('title') @translate(Brand Products) @stop

@section('content')

    <div class="ps-breadcrumb">
        <div class="ps-container">
            <ul class="breadcrumb">
                <li><a href="{{ route('homepage') }}">@translate(Home)</a></li>
                <li>@translate(Brand Products)</li>
            </ul>
        </div>
    </div>
    <div class="ps-page--shop mt-3">
        <div class="ps-container">

            {{--brand--}}
            <div class="ps-section__content d-none">
                <div class="ps-block--categories-tabs ps-tab-root">
                    <div class="ps-tabs">
                        <div class="ps-tabs">
                            <div class="ps-tab active">
                                <div class="ps-block__item">
                                    @forelse (brands(16) as $brand)
                                        <a href="{{ route('brand.shop', $brand->slug) }}">
                                            @if (empty($brand->logo))
                                                <img src="{{ asset('vendor-store.jpg') }}" class="rounded-lg" width="133" height="133"
                                                     alt="#{{ $brand->name }}">
                                            @else
                                                <img src="{{ filePath($brand->logo) }}" class="rounded-lg" width="133" height="133"
                                                     alt="#{{ $brand->name }}">
                                            @endif
                                        </a>
                                    @empty
                                        @translate(No Brand Found)
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--category--}}
            <div class="ps-shop-categories d-none">
                <div class="row align-content-lg-stretch">
                    @foreach (categories(10, null) as $home_category)
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--category-2" data-mh="categories">
                                <div class="ps-block__thumbnail"><img src="{{ filePath($home_category->image) }}"
                                                                      alt=""></div>
                                <div class="ps-block__content">
                                    <h4>{{ $home_category->name }}</h4>
                                    <ul>
                                        @foreach($home_category->childrenCategories as $parent_Cat)
                                            @foreach($parent_Cat->childrenCategories as $sub_cat)
                                                <li>
                                                    <a href="{{ route('category.shop',$sub_cat->slug) }}">{{ $sub_cat->name }}</a>
                                                </li>
                                            @endforeach
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="ps-layout--shop">
                <div class="ps-layout__left">
                    <aside class="widget widget_shop">
                        <h4 class="widget-title">@translate(Brands)</h4>
                        <ul class="ps-list--categories">

                            @foreach ($brand_products as $brand)
                                <li>
                                    <a href="{{ route('brand.shop', $brand->slug) }}">
                                        {{ $brand->name }}({{ $total_brand_product }})
                                    </a>
                                </li>
                            @endforeach

                        </ul>
                    </aside>
                </div>
                <div class="ps-layout__right">

                    <div class="ps-shopping ps-tab-root">
                        <div class="ps-shopping__header">
                            <p><strong> {{$total_brand_product}}</strong> @translate(Products found)</p>
                            <div class="ps-shopping__actions">
                                <div class="d-none">
                                    <select class="ps-select d-none" data-placeholder="Sort Items">
                                        <option>Sort by latest</option>
                                        <option>Sort by popularity</option>
                                        <option>Sort by average rating</option>
                                        <option>Sort by price: low to high</option>
                                        <option>Sort by price: high to low</option>
                                    </select>
                                </div>
                                <div class="ps-shopping__view">
                                    <p>@translate(View)</p>
                                    <ul class="ps-tab-list">
                                        <li class="active"><a href="#tab-1"><i class="icon-grid"></i></a></li>
                         
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="ps-tabs">
                            <div class="ps-tab active" id="tab-1">
                                <div class="ps-shopping-product">
                                    <div class="row no-gutters">

                                        @foreach($brand_products as $brand_product)
                                            @forelse($brand_product->products as $product)

                                                <div class="col-md-2 col-xl-2">
                                                    <x-product-card>
                                                        <x-slot name="product_id">{{ $product->id }}</x-slot>
                                                        <x-slot name="product_sku">{{ $product->sku }}</x-slot>
                                                        <x-slot name="product_slug">{{ $product->slug }}</x-slot>
                                                        <x-slot name="product_brand">{{ $product->brand->name }}</x-slot>
                                                        <x-slot name="brand_logo">{{ $product->brand->logo }}</x-slot>
                                                        <x-slot name="product_price">
                                                            {{ formatPrice(brandProductPrice($product->sellers)->min())
                                                                == formatPrice(brandProductPrice($product->sellers)->max())
                                                                ? formatPrice(brandProductPrice($product->sellers)->min())
                                                                : formatPrice(brandProductPrice($product->sellers)->min()).
                                                                '-' .formatPrice(brandProductPrice($product->sellers)->max()) }}
                                                        </x-slot>
                                                        <x-slot name="is_discount">{{ $product->is_discount }}</x-slot>
                                                        <x-slot name="discount_price">{{ $product->discount_price }}</x-slot>
                                                        <x-slot name="href">{{route('single.product',[$product->sku,$product->slug])}}</x-slot>
                                                        <x-slot name="image">{{ filePath($product->image)}}</x-slot>
                                                        <x-slot name="name">{{ $product->name }}</x-slot>
                                                    </x-product-card>
                                                </div>

                                                @empty

                                                <img src="{{ asset('no-product-found.png') }}" class="img-fluid" alt="#no-product-found">

                                            @endforelse
                                        @endforeach

                                    </div>
                                </div>


                            </div>
                       
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal d-none" id="shop-filter-lastest" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="list-group d-none">
                                <a class="list-group-item list-group-item-action" href="#">Sort
                                    by</a><a class="list-group-item list-group-item-action" href="#">Sort by average
                                    rating</a><a class="list-group-item list-group-item-action" href="#">Sort by
                                    latest</a><a class="list-group-item list-group-item-action" href="#">Sort by price:
                                    low to high</a><a class="list-group-item list-group-item-action" href="#">Sort by
                                    price: high to low</a><a class="list-group-item list-group-item-action text-center"
                                                             href="#" data-dismiss="modal"><strong>Close</strong></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
