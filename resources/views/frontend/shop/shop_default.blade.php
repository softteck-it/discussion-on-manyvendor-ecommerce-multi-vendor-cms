@extends('frontend.master')


@section('title','Shop')

@section('content')
<div class="container">

    {{-- breadcrumb --}}
    <div class="ps-breadcrumb">
        <div class="ps-container">
            <ul class="breadcrumb">
                <li><a href="{{ route('homepage') }}">@translate(Home)</a></li>
                <li>@translate(All Products)</li>
            </ul>
        </div>
    </div>
    {{-- breadcrumb::ENDS --}}
    
    <div class="ps-page--shop">
        <div class="ps-container">

            <div class="ps-block__header">
                <h3>@translate(New Brands)</h3>
            </div>

            {{-- BRANDS --}}
            <div class="ps-block__content t-pb-30">
                <div class="row no-gutters">
                    @forelse (brands(16) as $brand)
                    <div class="col-md-2 col-6">
                        <x-brand-card>
                            <x-slot name="brand_id">{{ $brand->id }}</x-slot>
                            <x-slot name="brand_slug">{{ $brand->slug }}</x-slot>
                            <x-slot name="brand_name">{{ $brand->name }}</x-slot>
                            <x-slot name="brand_logo">
                                @if (empty($brand->logo))
                                    {{asset('vendor-store.jpg')}}
                                @else
                                    {{ filePath($brand->logo) }}
                                @endif
                            </x-slot>
                            <x-slot name="brand_href">{{ route('brand.shop', $brand->slug) }}</x-slot>
                        </x-brand-card>
                    </div>
                    @endforeach
                </div>
            </div>
            {{-- BRANDS::ENDS --}}


            <div class="ps-layout--shop">
                <div class="">
                    <div class="ps-block--shop-features show-products-mobile">
                        
                        
                        <div class="my-container pl-0 pr-0">
                            <div class="ps-block__header">
                                <h3 class="text-white ml-2">@translate(Best Sale Items)</h3>
                            </div>

                            <div class="row no-gutters">
                            
                                    @foreach (sale_products(12) as $sale_items)
                                        @foreach ($sale_items->sale_products as $product)
                                        <div class="col-md-2 col-6">
                                            <x-product-card>
                                                <x-slot name="product_id">{{ $product->id }}</x-slot>
                                                <x-slot name="product_sku">{{ $product->sku }}</x-slot>
                                                <x-slot name="product_slug">{{ $product->slug }}</x-slot>
                                                <x-slot name="product_brand">{{ $product->brand->name }}</x-slot>
                                                <x-slot name="brand_logo">{{ $product->brand->logo }}</x-slot>
                                                <x-slot name="product_price">
                                                    {{ formatPrice(brandProductPrice($product->sellers)->min())
                                                        == formatPrice(brandProductPrice($product->sellers)->max())
                                                        ? formatPrice(brandProductPrice($product->sellers)->min())
                                                        : formatPrice(brandProductPrice($product->sellers)->min()).
                                                        '-' .formatPrice(brandProductPrice($product->sellers)->max()) }}
                                                </x-slot>
                                                <x-slot name="is_discount">{{ $product->is_discount }}</x-slot>
                                                <x-slot name="discount_price">{{ $product->discount_price }}</x-slot>
                                                <x-slot name="href">{{route('single.product',[$product->sku,$product->slug])}}</x-slot>
                                                <x-slot name="image">{{ filePath($product->image)}}</x-slot>
                                                <x-slot name="name">{{ $product->name }}</x-slot>
                                            </x-product-card>
                                        </div>
                                        @endforeach
                                    @endforeach
                            
                            </div>
                        </div>

                        <div class="ps-shopping ps-tab-root mt-3">
                            <div class="ps-shopping__header">
                                <p><strong> {{ total_products() }}</strong> @translate(Products found)</p>
                                <div class="ps-shopping__actions">
                                    <form action="{{ route('shop.filter') }}" method="GET" id="sort_form">
                                        <select class="ps-select" data-placeholder="Sort Items" name="sortby"
                                                id="sort_filter">
                                            <option value="latest">@translate(Sort by Latest)</option>
                                        </select>
                                    </form>
                                    <div class="ps-shopping__view">
                                        <p>View</p>
                                        <ul class="ps-tab-list">
                                            <li class="active"><a href="#tab-1"><i class="icon-grid"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="ps-tabs">
                                <div class="ps-tab active" id="tab-1">
                                        <div class="row no-gutters">
                                            @forelse(all_products() as $product)

                                                <div class="col-md-2 col-6 col-xl-2">
                                                    <x-product-card>
                                                        <x-slot name="product_id">{{ $product->id }}</x-slot>
                                                        <x-slot name="product_sku">{{ $product->sku }}</x-slot>
                                                        <x-slot name="product_slug">{{ $product->slug }}</x-slot>
                                                        <x-slot name="product_brand">{{ $product->brand->name }}</x-slot>
                                                        <x-slot name="brand_logo">{{ $product->brand->logo }}</x-slot>
                                                        <x-slot name="product_price">
                                                            {{ formatPrice(brandProductPrice($product->sellers)->min())
                                                                == formatPrice(brandProductPrice($product->sellers)->max())
                                                                ? formatPrice(brandProductPrice($product->sellers)->min())
                                                                : formatPrice(brandProductPrice($product->sellers)->min()).
                                                                '-' .formatPrice(brandProductPrice($product->sellers)->max()) }}
                                                        </x-slot>
                                                        <x-slot name="is_discount">{{ $product->is_discount }}</x-slot>
                                                        <x-slot name="discount_price">{{ $product->discount_price }}</x-slot>
                                                        <x-slot name="href">{{route('single.product',[$product->sku,$product->slug])}}</x-slot>
                                                        <x-slot name="image">{{ filePath($product->image)}}</x-slot>
                                                        <x-slot name="name">{{ $product->name }}</x-slot>
                                                    </x-product-card>
                                                </div>

                                            @empty
                                                <img src="{{ asset('no-product-found.png') }}" class="img-fluid" alt="#no-product-found">
                                            @endforelse

                                        </div>

                                    {{ all_products()->links('frontend.include.pagination.paginate_shop') }}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop