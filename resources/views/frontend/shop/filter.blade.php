@extends('frontend.master')

@section('title','Shop')

@section('content')
<div class="ps-breadcrumb">
       <div class="ps-container">
           <ul class="breadcrumb">
               <li><a href="{{ route('homepage') }}">@translate(Home)</a></li>
               <li>@translate(Shop)</li>
           </ul>
       </div>
   </div>
   <div class="ps-page--shop">
       <div class="ps-container">
           <div class="ps-shop-banner">
               <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on">
                 @foreach (categories(10, null) as $home_category)
                  @foreach($home_category->promotionBanner as $banner)
                      <a href="{{ $banner->link }}"><img src="{{ filePath($banner->image) }}" alt=""></a>
                  @endforeach
                 @endforeach
               </div>
           </div>
           <div class="ps-section__content">
               <div class="ps-block--categories-tabs ps-tab-root">
                   <div class="ps-tabs">
                       <div class="ps-tabs">
                           <div class="ps-tab active">
                               <div class="ps-block__item">
                                 @forelse (brands(16) as $brand)
                                     <a href="{{ route('brand.shop', $brand->slug) }}">
                                       @if (empty($brand->logo))
                                           <img src="{{ asset('vendor-store.jpg') }}" class="rounded" alt="#{{ $brand->name }}">
                                         @else
                                         <img src="{{ filePath($brand->logo) }}" class="rounded" alt="#{{ $brand->name }}">
                                       @endif
                                     </a>
                                 @empty
                                   @translate(No Brand Found)
                                 @endforelse
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>

           <div class="ps-shop-categories">
               <div class="row align-content-lg-stretch">
                 @foreach (categories(10, null) as $home_category)
                   <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 ">
                       <div class="ps-block--category-2" data-mh="categories">
                           <div class="ps-block__thumbnail"><img src="{{ filePath($home_category->image) }}" alt=""></div>
                           <div class="ps-block__content">
                               <h4>{{ $home_category->name }}</h4>
                               <ul>
                                 @foreach($home_category->childrenCategories as $parent_Cat)
                                   @foreach($parent_Cat->childrenCategories as $sub_cat)
                                     <li><a href="{{ route('category.shop',$sub_cat->slug) }}">{{ $sub_cat->name }}</a></li>
                                   @endforeach
                                 @endforeach
                               </ul>
                           </div>
                       </div>
                   </div>
                @endforeach
               </div>
           </div>
           <div class="ps-layout--shop">
               <div class="">


                   <div class="ps-shopping ps-tab-root">
                       <div class="ps-shopping__header">
                           <p><strong> {{ $filters->total() }}</strong> @translate(Products found)</p>
                           <div class="ps-shopping__actions">
                               <form action="{{ route('shop.filter') }}" method="GET" id="sort_form">
                                   <select class="ps-select" data-placeholder="Sort Items" name="sortby" id="sort_filter">
                                       <option value="latest">@translate(Sort by Latest)</option>

                                   </select>
                               </form>
                               <div class="ps-shopping__view">
                                   <p>@translate(View)</p>
                                   <ul class="ps-tab-list">
                                       <li class="active"><a href="#tab-1"><i class="icon-grid"></i></a></li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                       <div class="ps-tabs">
                           <div class="ps-tab active" id="tab-1">
                               <div class="ps-shopping-product">
                                   <div class="row">

                                     @forelse($filters as $product)

                                    <div class="col-md-2 col-xl-2 t-mb-30">
                                            <x-product-card>
                                                <x-slot name="product_id">{{ $product->id }}</x-slot>
                                                <x-slot name="product_sku">{{ $product->sku }}</x-slot>
                                                <x-slot name="product_slug">{{ $product->slug }}</x-slot>
                                                <x-slot name="product_brand">{{ $product->brand->name }}</x-slot>
                                                <x-slot name="brand_logo">{{ $product->brand->logo }}</x-slot>
                                                <x-slot name="product_price">
                                                    {{ formatPrice(brandProductPrice($product->sellers)->min())
                                                        == formatPrice(brandProductPrice($product->sellers)->max())
                                                        ? formatPrice(brandProductPrice($product->sellers)->min())
                                                        : formatPrice(brandProductPrice($product->sellers)->min()).
                                                        '-' .formatPrice(brandProductPrice($product->sellers)->max()) }}
                                                </x-slot>
                                                <x-slot name="is_discount">{{ $product->is_discount }}</x-slot>
                                                <x-slot name="discount_price">{{ $product->discount_price }}</x-slot>
                                                <x-slot name="href">{{route('single.product',[$product->sku,$product->slug])}}</x-slot>
                                                <x-slot name="image">{{ filePath($product->image)}}</x-slot>
                                                <x-slot name="name">{{ $product->name }}</x-slot>
                                            </x-product-card>
                                    </div>

                                                        
                                       @empty
                                       <div class="text-center">
                                           <h3>@translate(No Product Found)</h3>
                                       </div>
                                     @endforelse

                                   </div>
                               </div>

                               {{ $filters->links('frontend.include.pagination.paginate_shop') }}

                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

@stop

