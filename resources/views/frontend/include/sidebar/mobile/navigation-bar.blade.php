<div class="navigation--list">
    <div class="navigation__content">
        <a class="navigation__item ps-toggle--sidebar" href="#menu-mobile">
            <lord-icon
                src="https://cdn.lordicon.com/sbiheqdr.json"
                trigger="loop">
            </lord-icon>
            <span> @translate(Menu)</span>
        </a>

        <a class="navigation__item ps-toggle--sidebar" href="#navigation-mobile">
            <lord-icon
                src="https://cdn.lordicon.com/bkgjmybn.json"
                trigger="loop"
                colors="primary:#121331,secondary:#000000">
            </lord-icon>
            <span> @translate(Categories)</span>
        </a>

        <a
            class="navigation__item ps-toggle--sidebar" href="#search-sidebar">
            <lord-icon
                src="https://cdn.lordicon.com/msoeawqm.json"
                trigger="loop"
                delay="1000">
            </lord-icon>
            <span> @translate(Search)</span>
        </a>

        
        <a
            class="navigation__item"
            href="@auth {{route('customer.track.order')}} @else {{route('login')}}  @endauth">
            <lord-icon
                src="https://cdn.lordicon.com/uetqnvvg.json"
                trigger="loop"
                colors="primary:#121331,secondary:#000000">
            </lord-icon>
            <span> @translate(Track Order)</span>
        </a>

        <a
            class="navigation__item ps-toggle--sidebar"
            href="#cart-mobile">
            <lord-icon
                src="https://cdn.lordicon.com/slkvcfos.json"
                trigger="loop"
                colors="primary:#121331,secondary:#000000"
                stroke="50">
            </lord-icon>
            
            <span> @translate(Cart)</span>
        </a>

    </div>
</div>
