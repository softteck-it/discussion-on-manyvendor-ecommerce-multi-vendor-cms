<header class="header header--market-place-3" data-sticky="true">
    <div class="nav-top">
        <div class="container">
            <div class="row">
                <div class="col-6 text-left">
                    <div class="news">
                        <span>@translate(campaigns)</span>
                        <ul class="scrollLeft">
                            @forelse(activeCampaign() as $campaign)
                                <li><a href="{{route('customer.campaign.products', $campaign->slug)}}">{{$campaign->title}}</a></li>
                            @empty
                            @endforelse
                        </ul>
                    </div>
                </div>
                <div class="col-6 text-right">
                    <ul class="navigation__extra">
                        @if(deliverActive())
                        <li><a href="{{ route('deliver.signup') }}">@translate(Be a Delivery Man)</a></li>
                        @endif
                        @if(affiliateRoute() && affiliateActive())
                        @auth
                        @if(Auth::user()->user_type != "Admin" && Auth::user()->user_type != "Vendor")
                        <li><a href="{{ route('customers.affiliate.registration') }}">@translate(Affiliate Marketing)</a></li>
                        @endif
                        @endauth
                        @guest
                        <li><a href="{{ route('customers.affiliate.registration') }}">@translate(Affiliate Marketing)</a></li>
                        @endguest
                        @endif
                        @if(vendorActive())
                        <li><a href="{{ route('vendor.signup') }}">@translate(Be a seller)</a></li>

                        @endif
                        <li>
                            <div class="ps-dropdown"><a href="javascript:;">{{Str::ucfirst(defaultCurrency())}}</a>
                                <ul class="ps-dropdown-menu  dropdown-menu-right">
                                    @foreach(\App\Models\Currency::where('is_published',true)->get() as $item)
                                    <li><a class="dropdown-item" href="{{route('frontend.currencies.change')}}" onclick="event.preventDefault();
                                                       document.getElementById('{{$item->name}}').submit()">
                                            <img width="25" height="auto"
                                                src="{{ asset("images/lang/". $item->image) }}" alt="" />
                                            {{$item->name}}</a>
                                        <form id="{{$item->name}}" class="d-none"
                                            action="{{ route('frontend.currencies.change') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="code" value="{{$item->id}}">
                                        </form>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                        @if(config('manyvendor.google_translate') == 'YES')
                        <li>
                            <div id="google_translate_element"></div>
                        </li>
                        @else
                        <li>
                            <div class="ps-dropdown language"><a
                                    href="javascript:;">{{Str::ucfirst(\Illuminate\Support\Facades\Session::get('locale') ?? env('DEFAULT_LANGUAGE'))}}</a>
                                <ul class="ps-dropdown-menu  dropdown-menu-right">
                                    @foreach(\App\Models\Language::all() as $language)
                                    <li><a class="dropdown-item" href="{{route('frontend.language.change')}}" onclick="event.preventDefault();
                                                       document.getElementById('{{$language->name}}').submit()">
                                            <img width="25" height="auto"
                                                src="{{ asset("images/lang/". $language->image) }}" alt="" />
                                            {{$language->name}}</a>
                                        <form id="{{$language->name}}" class="d-none"
                                            action="{{ route('frontend.language.change') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="code" value="{{$language->code}}">
                                        </form>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="header__top">
        <div class="container">
            <div class="header__left">
                <div class="menu--product-categories">
                    <div class="menu__toggle">
                        <i class="icon-menu"></i><span> @translate(Categories)</span>
                    </div>
                    <div class="menu__content">
                        <div class="v3-categories">

                            <!-- start -->
                            <ul class="cat-wrapper">

                                @foreach(categories(100,null) as $gCat)
                                @if($gCat->childrenCategories->count() > 0)
                                <li class="has-sublist d-flex align-items-center"> <i class="{{ $gCat->icon }}"></i><a
                                        href="{{ route('category.shop',$gCat->slug) }}">{{ trans('categories.'. $gCat->name) }}
                                        <span class="arrow-icon"><i class="fas fa-chevron-right"></i></span></a>

                                    <div class="mega-wrapper">

                                        <ul class="mega-menu">
                                            @foreach($gCat->childrenCategories as $pCat)
                                            @if($pCat->childrenCategories->count() > 0)
                                            <li class="mega-item">
                                                <ul>

                                                    <li class="title mt-10">{{ trans('categories.'. $pCat->name) }}</li>
                                                    @foreach($pCat->childrenCategories as $sCat)
                                                    <li><a
                                                            href="{{ route('category.shop',$sCat->slug) }}">{{ trans('categories.'. $sCat->name) }}</a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                            @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                                @endif
                                @endforeach
                            </ul>
                            <!-- end -->
                        </div>
                    </div>
                </div>

                <a class="ps-logo" href="{{ route('homepage') }}">
                    <img src="{{ filePath(getSystemSetting('type_logo'))}}" class="" width="150" alt="">
                </a>
            </div>
            <div class="header__center position-relative">
                <form class="ps-form--quick-search" id="search-form" action="{{route('product.search')}}" method="get">
                    <span class="position-relative w-100">
                        <span class="mic-icon-postion start-record-btn"><i class="fas fa-microphone-alt"></i></span>
                        <span class="mic-icon-postion d-none pause-record-btn"><i class="fas fa-microphone-alt-slash"></i></span>
                        <input class="form-control voice_input" name="key" id="filter_input" type="text" value="{{Request::get('key')}}"
                            placeholder="@translate(I am looking for)..." autofocus>
                    </span>


                    <div class="form-group--icon w-50"><i class="icon-chevron-down"></i>
                        <input type="hidden" id="filter_url" value="{{ route('header.search') }}">

                        <select class="form-control" name="filter_type" id="filter_type">
                            <option value="product" selected>@translate(Product)</option>
                            @if(vendorActive())
                            <option value="shop">@translate(Shop)</option>
                            @endif
                        </select>
                    </div>
                    <button type="submit">@translate(Search)</button>
                </form>

                {{-- Search result --}}
                <div class="search-table d-none">
                    <div class="row m-auto p-3 no-gutters" id="show_data">
                        {{-- Data goes here --}}
                    </div>
                </div>
                {{-- Search result:END --}}
            </div>
            <div class="header__right">
                <div class="header__actions">

                    <div class="ps-cart--mini"><a class="header__extra" href="javascript:;"><i
                                class="las la-exchange-alt"></i><span><i
                                    class="navbar-comparison">@translate(0)</i></span></a>
                        <div class="ps-cart__content">
                            <div class="ps-cart__items">
                                <div class="mb-3">@translate(Comparison Items)</div>
                                <span class="show-comparison-items">
                                    {{-- data coming from ajax --}}
                                </span>
                            </div>
                            <div class="ps-cart__footer comparison-items-footer">
                                {{-- data coming from ajax --}}
                            </div>
                        </div>
                    </div>

                    <div class="ps-cart--mini"><a class="header__extra" href="#"><i
                                class="las la-shopping-bag"></i><span><i
                                    class="navbar-cart">@translate(0)</i></span></a>
                        <div class="ps-cart__content">
                            <div class="ps-cart__items h-600">
                                <div class="mb-2">@translate(Cart Items)</div>
                                <span class="show-cart-items">
                                    {{-- data coming from ajax --}}
                                </span>
                            </div>
                            @auth
                            <div class="ps-cart__footer cart-items-footer">
                                {{-- data coming from ajax --}}
                            </div>
                            @endauth
                            @guest
                            <div class="ps-cart__footer cart-items-footer">

                            </div>
                            @endguest
                        </div>
                    </div>


                    <div class="ps-block--user-header">
                        @if (Route::has('login'))
                        @auth
                        @if (Auth::user()->user_type == 'Customer')
                        <a href="{{ route('customer.index') }}">

                            @if (Str::substr(Auth::user()->avatar, 0, 7) == 'uploads')

                            <img src="{{ filePath(Auth::user()->avatar) }}"
                                class="ps-block--user-header-img w-100 img-fluid" alt="{{ Auth::user()->name }}">
                            @else
                            <img src="{{ asset(Auth::user()->avatar) }}"
                                class="ps-block--user-header-img w-100 img-fluid" alt="{{ Auth::user()->name }}">
                            @endif


                        </a>
                        @else
                        <a href="{{ route('home') }}">
                            <img src="{{ filePath(Auth::user()->avatar) }}"
                                class="ps-block--user-header-img w-100 img-fluid" alt="{{ Auth::user()->name }}">
                        </a>
                        @endif

                        @else
                        <div class=" ps-block__left">
                            <i class="las la-user"></i></div>

                        <div class="ps-block__right">
                            <a href="{{ route('login') }}">@translate(Login)</a></div>
                        @endauth
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <nav class="navigation">
        <div class="container">
            <div class="navigation__left">
                <div class="menu--product-categories">
                    <div class="menu__toggle active"><i class="icon-menu"></i><span> @translate(Categories)</span></div>
                    <div class="menu__content">
                        <div class="v3-categories">

                            <!-- start -->
                            <ul class="cat-wrapper">

                                @foreach(categories(100,null) as $gCat)
                                @if($gCat->childrenCategories->count() > 0)
                                <li class="has-sublist d-flex align-items-center"> <i class="{{ $gCat->icon }}"></i><a
                                        href="{{ route('category.shop',$gCat->slug) }}">{{ trans('categories.'. $gCat->name) }}
                                        <span class="arrow-icon"><i class="fas fa-chevron-right"></i></span></a>

                                    <div class="mega-wrapper">

                                        <ul class="mega-menu">
                                            @foreach($gCat->childrenCategories as $pCat)
                                            @if($pCat->childrenCategories->count() > 0)
                                            <li class="mega-item">
                                                <ul>
                                                    <li class="title mt-10">{{ trans('categories.'. $pCat->name) }}</li>
                                                    @foreach($pCat->childrenCategories as $sCat)
                                                    <li><a
                                                            href="{{ route('category.shop',$sCat->slug) }}">{{ trans('categories.'. $sCat->name) }}</a>
                                                    </li>
                                                    @endforeach

                                                </ul>

                                            </li>

                                            @endif
                                            @endforeach

                                        </ul>

                                    </div>

                                </li>
                                @endif
                                @endforeach
                            </ul>
                            <!-- end -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="navigation__right">
                <ul class="menu menu--recent-view">
                    <li><a href="{{ route('all.product') }}">@translate(All Products)</a></li>

                    @if(vendorActive())
                    <li><a href="{{ route('vendor.shops') ?? '' }}">@translate(All Shops)</a></li>
                    @endif

                    <li><a href="{{ route('brands') ?? '' }}">@translate(All Brands)</a></li>

                    <li><a href="{{ route('customer.campaigns.index') }}">@translate(Campaigns)</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

{{-- SIDEBAR --}}

<div class="m_icons_sidebar wow fadeInRight">
    <div class="position-class">
        {{-- middle icons --}}
        <ul class="middle-icons-wrapper">

            <li data-toggle="tooltip" data-placement="left" title="Wishlist">
                <span class="badge-one" id="listitem">0</span>
                <a href="{{ route('wishlists.index') }}">
                    <lord-icon
                        src="https://cdn.lordicon.com/rjzlnunf.json"
                        trigger="loop"
                        colors="primary:#ffffff,secondary:#ee6d66"
                        stroke="100">
                    </lord-icon>
                </a>
            </li>

            @auth
            <li data-toggle="tooltip" data-placement="left" title="Track Order">
                <a href="{{ route('customer.track.order') }}">
                    <lord-icon
                        src="https://cdn.lordicon.com/poblyvkl.json"
                        trigger="loop"
                        colors="primary:#ffffff,secondary:#08a88a"
                        stroke="100">
                    </lord-icon>
                </a>
            </li>
            @else
            <li data-toggle="tooltip" data-placement="left" title="Track Order">
                <a href="{{ route('login') }}">
                    <i class="las la-truck-moving"></i>
                </a>
            </li>
            @endauth

        </ul>
        {{-- bottom icons --}}
        <div class="bottom-icons-wrapper middle-icons-wrapper">

            @if(getSystemSetting('type_fb'))
            <li data-toggle="tooltip" data-placement="left" title="Facebook"><a href="{{getSystemSetting('type_fb')}}" target="_blank"><i
                        class="fab fa-facebook-f"></i></a></li>
            @endif

            @if(getSystemSetting('type_tw'))
            <li data-toggle="tooltip" data-placement="left" title="Twitter"><a href="{{getSystemSetting('type_tw')}}" target="_blank"><i
                        class="fab fa-twitter"></i></a></li>
            @endif

            @if(getSystemSetting('type_google'))
            <li data-toggle="tooltip" data-placement="left" title="Google"><a href="{{getSystemSetting('type_google')}}" target="_blank"><i
                        class="fab fa-google"></i></a></li>
            @endif

        </div>
    </div>
</div>

@if (getPopup('sidepopup'))
<div class="m_position-img active wow bounceIn">
    {{-- main image --}}
    <a href="{{ getPopup('sidepopup')->link }}">
        <img class="float-img" src="{{ filePath(getPopup('sidepopup')->image) }}" alt="">
    </a>

    {{-- close btn --}}
    <img class="float-close-btn" src="https://img.alicdn.com/tfs/TB1tH8.d3gP7K4jSZFqXXamhVXa-52-52.png" alt="">
</div>
@endif

{{-- SIDEBAR::ENDS --}}

<script src="{{ asset('frontend\js\typewriter.js') }}"></script>

<script>
    $(document).ready(function () {

        // TypeWriter
        new TypeWriter('#filter_input', [
            'Search for a product',
            'Bike',
            'Egg',
            'T-shirt',
            'Shoes',
            'Bag',
            'Watch',
            'Phone',
            'Laptop',
            'Camera',
            'Keyboard',
            'Mouse',
            'Speaker',
            'Headphone',
            'Earphone',
            ], {
            holdOnceWritten: 1000,
            holdOnceDeleted: 1000
        });

        // search
        $('#filter_input').on('keyup', function () {

            var url = $('#filter_url').val();
            var type = $('#filter_type').val();
            var input = $('#filter_input').val();

            /*ajax get value*/
            if (url === null) {
                // location.reload()
            } else {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: url,
                    method: 'GET',
                    data: {
                        type: type,
                        input: input
                    },
                    success: function (result) {
                        if (input === null || input === '') {
                            $('#show_data').addClass('d-none');
                            $('.search-table').addClass('d-none');
                        } else {
                            $('#show_data').html(result);
                            $('#show_data').removeClass('d-none');
                            $('.search-table').removeClass('d-none');
                        }
                    }
                });


            }
        })
    });


    function CompareFormSubmit() {
        $('#comparison_form').submit();
    }

</script>
