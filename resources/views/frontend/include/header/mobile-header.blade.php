<header class="header header--mobile" data-sticky="true">
    <div class="navigation--mobile">
        <div class="navigation__left"><a class="ps-logo" href="{{ route('homepage') }}"><img
                    src="{{ filePath(getSystemSetting('type_logo'))}}" class="" alt=""></a></div>
        <div class="navigation__right">
            <div class="header__actions">
            
                <div class="ps-block--user-header">
                        @auth
                            <div class="ps-block__left">
                                @if (Auth::user()->user_type == 'Customer')
                                    <a href="{{ route('customer.index') }}">
                                        <img src="{{ filePath(Auth::user()->avatar) }}" class="ps-block--user-header-img w-100 img-fluid" alt="">
                                    </a>
                                @elseif (Auth::user()->user_type == 'Admin')
                                    <a href="{{ route('home') }}">
                                        <img src="{{ filePath(Auth::user()->avatar) }}" class="ps-block--user-header-img w-100 img-fluid" alt="">
                                    </a>
                                @elseif (Auth::user()->user_type == 'Vendor')
                                    <a href="{{ route('home') }}">
                                        <img src="{{ filePath(Auth::user()->avatar) }}" class="ps-block--user-header-img w-100 img-fluid" alt="">
                                    </a>
                                @else
                                    <lord-icon
                                        src="https://cdn.lordicon.com/imamsnbq.json"
                                        trigger="loop"
                                        delay="2000"
                                        stroke="80"
                                        colors="primary:#000,secondary:#000">
                                    </lord-icon>
                                @endif
                            </div>
                        @endauth

                        @guest
                        <div class="ps-block__left">
                            <a href="{{ route('login') }}">
                                <lord-icon
                                    src="https://cdn.lordicon.com/imamsnbq.json"
                                    trigger="loop"
                                    delay="2000"
                                    stroke="80"
                                    style="width: 40px; height: 40px;"
                                    colors="primary:#000,secondary:#000">
                                </lord-icon>
                            </a>
                        </div>
                        @endguest
                </div>
            </div>
        </div>
    </div>
   
</header>
