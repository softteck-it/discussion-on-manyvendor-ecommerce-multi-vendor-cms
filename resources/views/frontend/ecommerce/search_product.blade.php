@extends('frontend.master')


@section('title','Search product')

@section('content')

    <div class="ps-breadcrumb">
        <div class="ps-container">
            <ul class="breadcrumb">
                <li><a href="{{ route('homepage') }}">@translate(Home)</a></li>
                <li>@translate(Shop)</li>
            </ul>
        </div>
    </div>
    <div class="ps-page--shop mt-3">
        <div class="ps-container">
       
            <div class="ps-layout--shop">
                <div class="ps-layout__left">
                    <aside class="widget widget_shop">
                        <h4 class="widget-title">@translate(Categories)</h4>
                        <ul class="ps-list--categories">

                            @foreach (categories(10, null) as $home_category)
                                <li class="current-menu-item menu-item-has-children"><a
                                            href="{{ route('category.shop', $home_category->slug) }}">{{ $home_category->name }}</a><span
                                            class="sub-toggle"><i class="fa fa-angle-down"></i></span>
                                    <ul class="sub-menu">
                                        @foreach($home_category->childrenCategories as $parent_Cat)
                                            @foreach($parent_Cat->childrenCategories as $sub_cat)
                                                <li class="current-menu-item "><a
                                                            href="{{ route('category.shop',$sub_cat->slug) }}">{{ $sub_cat->name }}
                                                        ({{ App\Models\Product::where('category_id', $sub_cat->id)->count() }})</a>
                                                </li>
                                            @endforeach
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    </aside>
                </div>
                <div class="ps-layout__right">
                  
                    <div class="ps-shopping ps-tab-root">
                        <div class="ps-shopping__header">
                            <p><strong> {{$total_product}}</strong> @translate(Products found) <small>@translate(Search keyword) : {{$key}}</small></p>
                            <div class="ps-shopping__actions">
                           
                                <div class="ps-shopping__view">
                                    <p>@translate(View)</p>
                                    <ul class="ps-tab-list">
                                        <li class="active"><a href="#tab-1"><i class="icon-grid"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="ps-tabs">
                            <div class="ps-tab active" id="tab-1">
                                <div class="ps-shopping-product">
                                    <div class="row">
                                        @foreach($results as $product)


                                            <div class="col-md-3 col-xl-2 t-mb-30">
                                                <a href="{{ route('single.product',[$product->sku,$product->slug]) }}" class="product-card">
                                                                <span class="product-card__action d-flex flex-column align-items-center ">
                                                                    <span class="product-card__action-is product-card__action-view"
                                                                          onclick="forModal('{{ route('quick.view',$product->slug) }}', '@translate(Product quick view)')">
                                                                    <i class="fa fa-eye"></i>
                                                                    </span>
                                                                    <span class="product-card__action-is product-card__action-compare"
                                                                          onclick="addToCompare({{$product->id}})">
                                                                    <i class="fa fa-random"></i>
                                                                    </span>
                                                                    @auth()
                                                                        <span class="product-card__action-is product-card__action-wishlist"
                                                                              onclick="addToWishlist({{$product->id}})">
                                                                    <i class="fas fa-heart"></i>
                                                                    </span>
                                                                    @endauth

                                                                    @guest()
                                                                        <a href="{{ route('login') }}"
                                                                                class="product-card__action-is product-card__action-wishlist"
                                                                                
                                                                        >
                                                                    <i class="fas fa-heart"></i>
                                                                </a>
                                                                    @endguest





                                                                </span>
                                                    <span class="product-card__img-wrapper">
                                                                    <img src="{{ filePath($product->image) }}" alt="manyvendor" class="img-fluid mx-auto">
                                                                </span>
                                                    <span class="product-card__body">
                                                                    <span class="product-card__title">
                                                                        {{ $product->name }}
                                                                    </span>

                                                                    <span class="t-mt-10 d-block">
                                                                    <span class="product-card__discount-price t-mr-5">
                                                                      @if($product->is_discount)
                                                                            <p class="ps-product__price sale">{{formatPrice($product->discount_price)}}
                                                                        <del>
                                                                            {{formatPrice($product->product_price)}}</del>
                                                                    </p>
                                                                        @else
                                                                            <p class="ps-product__price">{{formatPrice($product->product_price)}}</p>
                                                                        @endif
                                                                    </span>

                                                                    </span>

                                                                </span>
                                                </a>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal d-none" id="shop-filter-lastest" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="list-group d-none"><a class="list-group-item list-group-item-action" href="#">@translate(Sort by)</a>
                                <a class="list-group-item list-group-item-action" href="#">@translate(Sort by average rating)</a>
                                <a class="list-group-item list-group-item-action" href="#">@translate(Sort by latest)</a>
                                <a class="list-group-item list-group-item-action" href="#">@translate(Sort by price: low to high)</a>
                                <a class="list-group-item list-group-item-action" href="#">@translate(Sort by price: high to low)</a>
                                <a class="list-group-item list-group-item-action text-center" href="#" data-dismiss="modal"><strong>@translate(Close)</strong></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

