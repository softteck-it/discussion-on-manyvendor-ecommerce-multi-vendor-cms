@extends('frontend.master')


@section('title','All product')

@section('content')
<div class="container">

<div class="ps-breadcrumb">
    <div class="ps-container">
        <ul class="breadcrumb">
            <li><a href="{{ route('homepage') }}">@translate(Home)</a></li>
            <li>@translate(All product)</li>
        </ul>
    </div>
</div>
<div class="ps-page--shop">
    <div class="ps-container">
        

        {{-- BRANDS --}}
            <div class="ps-block__content t-pb-30">
                <div class="row no-gutters">
                    @forelse (brands(16) as $brand)
                    <div class="col-md-2">
                        <x-brand-card>
                            <x-slot name="brand_id">{{ $brand->id }}</x-slot>
                            <x-slot name="brand_slug">{{ $brand->slug }}</x-slot>
                            <x-slot name="brand_name">{{ $brand->name }}</x-slot>
                            <x-slot name="brand_logo">
                                @if (empty($brand->logo))
                                    {{asset('vendor-store.jpg')}}
                                @else
                                    {{ filePath($brand->logo) }}
                                @endif
                            </x-slot>
                            <x-slot name="brand_href">{{ route('brand.shop', $brand->slug) }}</x-slot>
                        </x-brand-card>
                    </div>
                    @endforeach
                </div>
            </div>
            {{-- BRANDS::ENDS --}}


        <div class="ps-layout--shop">
            <div class="mt-4">
                <div class="ps-block--shop-features show-products-mobile">
                    <div class="ps-block__header">
                        <h3>@translate(Best Sale Items)</h3>
                        <div class="ps-block__navigation"><a class="ps-carousel__prev" href="#recommended1"><i
                                    class="icon-chevron-left"></i></a><a class="ps-carousel__next"
                                href="#recommended1"><i class="icon-chevron-right"></i></a></div>
                    </div>

                    @if (sale_products(10)->count() > 0)

                    <div class="ps-block__content">
                        <div class="owl-slider" id="recommended1" data-owl-auto="true" data-owl-loop="true"
                            data-owl-speed="10000" data-owl-gap="30" data-owl-nav="false" data-owl-dots="false"
                            data-owl-item="6" data-owl-item-xs="2" data-owl-item-sm="2" data-owl-item-md="3"
                            data-owl-item-lg="4" data-owl-item-xl="5" data-owl-duration="1000" data-owl-mousedrag="on">

                            @foreach (sale_products(10) as $sale_item)
                            @if($sale_item->is_discount)
                            <div class="ps-product">
                                <div class="ps-product__thumbnail">
                                    <a href="{{ route('single.product',[$sale_item->sku,$sale_item->slug]) }}">
                                        <img src="{{ filePath($sale_item->image) }}" alt="#{{ $sale_item->name }}">
                                    </a>
                                    <div class="ps-product__badge">
                                        @if($sale_item->is_discount)
                                        {{formatPrice($sale_item->discount_percentage)}}%
                                        @endif
                                    </div>
                                </div>
                                <div class="ps-product__container">
                                    <div class="ps-product__content"><a class="ps-product__title"
                                            href="{{ route('single.product',[$sale_item->sku,$sale_item->slug]) }}">{{ $sale_item->name }}</a>
                                        @if($sale_item->is_discount)
                                        <p class="ps-product__price sale">{{formatPrice($sale_item->discount_price)}}
                                            <del>
                                                {{formatPrice($sale_item->product_price)}}</del>
                                        </p>
                                        @else
                                        <p class="ps-product__price">{{formatPrice($sale_item->product_price)}}</p>
                                        @endif
                                    </div>
                                    <div class="ps-product__content hover"><a class="ps-product__title"
                                            href="{{ route('single.product',[$sale_item->sku,$sale_item->slug]) }}">{{ $sale_item->name }}</a>
                                        @if($sale_item->is_discount)
                                        <p class="ps-product__price sale">{{formatPrice($sale_item->discount_price)}}
                                            <del>
                                                {{formatPrice($sale_item->product_price)}}</del>
                                        </p>
                                        @else
                                        <p class="ps-product__price">{{formatPrice($sale_item->product_price)}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach

                        </div>
                    </div>

                    @else
                    <img src="{{ asset('no-product-found.png') }}" class="w-100" alt="#No product found">
                    @endif

                    <div class="ps-shopping ps-tab-root">
                        <div class="ps-shopping__header">
                            <p><strong> {{ total_products() }}</strong> @translate(Products found)</p>
                            <div class="ps-shopping__actions ">
                                <form action="{{ route('shop.filter') }}" method="GET" id="sort_form" class="d-none">
                                    <select class="ps-select" data-placeholder="Sort Items" name="sortby"
                                        id="sort_filter">
                                        <option value="latest">@translate(Sort by Latest)</option>
                                    </select>
                                </form>
                                <div class="ps-shopping__view">
                                    <p>View</p>
                                    <ul class="ps-tab-list">
                                        <li class="active"><a href="#tab-1"><i class="icon-grid"></i></a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="ps-tabs">
                            <div class="ps-tab active" id="tab-1">
                                <div class="ps-shopping-product">
                                    <div class="row no-gutters">
                                        @forelse(all_products() as $product)

                                        <div class="col-md-2 col-2 col-xl-2 t-mb-30">
                                            <x-product-card>
                                                <x-slot name="product_id">{{ $product->id }}</x-slot>
                                                <x-slot name="product_sku">{{ $product->sku }}</x-slot>
                                                <x-slot name="product_slug">{{ $product->slug }}</x-slot>
                                                <x-slot name="product_brand">{{ $product->brand->name }}</x-slot>
                                                <x-slot name="brand_logo">{{ $product->brand->logo }}</x-slot>
                                                @if (vendorActive())
                                                <x-slot name="product_price">
                                                    {{ formatPrice(brandProductPrice($product->sellers)->min())
                                                        == formatPrice(brandProductPrice($product->sellers)->max())
                                                        ? formatPrice(brandProductPrice($product->sellers)->min())
                                                        : formatPrice(brandProductPrice($product->sellers)->min()).
                                                        '-' .formatPrice(brandProductPrice($product->sellers)->max()) }}
                                                </x-slot>
                                                @else
                                                    <x-slot name="product_price">
                                                        {{formatPrice($product->product_price)}}
                                                    </x-slot>

                                                <x-slot name="is_discount">{{ $product->is_discount == null ? '0' : '1' }}</x-slot>
                                                <x-slot name="discount_price">{{ $product->discount_price }}</x-slot>

                                                @endif
                                                <x-slot name="href">{{route('single.product',[$product->sku,$product->slug])}}</x-slot>
                                                <x-slot name="image">{{ filePath($product->image)}}</x-slot>
                                                <x-slot name="name">{{ $product->name }}</x-slot>
                                            </x-product-card>
                                        </div>


                                        @empty
                                        <img src="{{ asset('no-product-found.png') }}" class="w-100"
                                            alt="#No product found">
                                        @endforelse

                                    </div>
                                </div>

                                {{ all_products()->links('frontend.include.pagination.paginate_shop') }}

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

    @stop