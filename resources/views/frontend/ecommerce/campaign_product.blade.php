@extends('frontend.master')


@section('title') @translate(Campaigns) @stop

@section('content')
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{route('homepage')}}">@translate(Home)</a></li>
                <li><a href="{{route('customer.campaigns.index')}}">@translate(Campaigns)</a></li>
                <li>{{$campaign->title}}</li>
            </ul>
        </div>
    </div>

    <div class="mb-5">
        <div class="container my-container">
            <div class="d-flex justify-content-center fs-32">
                <span class="text-white">@translate(All products of) {{$campaign->title}}</span>
            </div>
            <div class="row mt-5 no-gutters">
                @forelse($product_list as $product)
                    <div class="col-md-3 col-6 mb-2">

                        <div class="product-container">
                            <a href="javascript:;" title="{{$product->name}}" onclick="forModal('{{ route('quick.view',$product->slug) }}', '@translate(Product quick view)')">
                                <div class="card card-body bd-light rounded-sm">

                                    <div class="product-image-container" style="padding-bottom: 100%;">
                                        <img class="product-image" src="{{$product->image}}" alt="{{$product->name}}">
                                    </div>

                                    <div class="product-dscr-container text-center">
                                        {{$product->name}}
                                    </div>

                                    <div class="product-price text-center">
                                        {{$product->price}}
                                    </div>
                                  

                                    @if($product->have_variant)
                                        <div class="card-footer text-center border-top-0">
                                            <a href="#" class="btn btn-primary m-2 p-3 fs-12 w-full"
                                            onclick="forModal('{{route('product.with.variant',[$product->product_id,$product->campaign_id])}}',@translate(Select Variant))">@translate(Buy Now)</a>
                                        </div>

                                    @else
                                        @auth()
                                            <input type="hidden" class="cart-quantity-{{$product->product_variant_stock_id}}" value="1" min="1">
                                            <div class="card-footer bf-white text-center border-top-0">
                                                <a href="#!"
                                                class="btn btn-primary m-2 p-3 w-100 fs-12 addToCart-{{$product->vendor_product_variant_stock_id}}"
                                                onclick="addToCart('{{$product->product_variant_stock_id}}','{{$product->campaign_id}}')">@translate(Buy Now)</a>
                                            </div>
                                        @endauth
                                        @guest()
                                                <input type="hidden" class="cart-quantity-{{$product->product_variant_stock_id}}" value="1" min="1">
                                                <div class="card-footer text-center border-top-0">
                                                    <a href="#!"
                                                    class="btn btn-primary m-2 w-100 p-3 fs-12 addToCart-{{$product->product_variant_stock_id}}"
                                                    onclick="addToGuestCart('{{$product->product_variant_stock_id}}','{{$product->campaign_id}}')">@translate(Buy Now)</a>
                                                </div>
                                        @endguest
                                    @endif
                                </div>
                            </a>
                        </div>
                    </div>
                @empty
                    <div class="col-md-12 text-center text-danger fs-18 py-5 card card-body">
                        @translate(There is no product available for this campaign now.)
                    </div>
                @endforelse
            </div>

        </div>
    </div>
@stop


