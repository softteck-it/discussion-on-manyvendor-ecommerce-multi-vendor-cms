@extends('frontend.master')

@section('title'){{ $seller_store->shop_name }}@stop

@section('content')

    <!-- breadcrumb:START -->
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('homepage') }}">@translate(Home)</a></li>
                <li><a href="#">{{ $seller_store->shop_name }}</a></li>
            </ul>
        </div>
    </div>
    <!-- breadcrumb:END -->

    <!-- SHOP:START -->
    <div class="ps-vendor-store">
        <div class="container">
            <div class="ps-section__container">
                <div class="ps-section__right">
                    <!-- SELLER PRODUCTS -->
                    <div class="ps-shopping ps-tab-root">
                        <div class="ps-shopping__header">
                            <p><strong> {{ $products->count() }}</strong> @translate(Products found)</p>
                            <div class="ps-shopping__actions">
                                <div class="d-none">
                                    <select class="ps-select" data-placeholder="Sort Items">
                                        <option>Sort by latest</option>
                                        <option>Sort by popularity</option>
                                        <option>Sort by average rating</option>
                                        <option>Sort by price: low to high</option>
                                        <option>Sort by price: high to low</option>
                                    </select>
                                </div>
                                <div class="ps-shopping__view">
                                    <p>@translate(View)</p>
                                    <ul class="ps-tab-list">
                                        <li class="active"><a href="#tab-1"><i class="icon-grid"></i></a></li>
                                  
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="ps-tabs" id="myStore">
                            <div class="ps-tab active" id="tab-1">
                                <div class="row no-gutters">

                                    @forelse($products as $product)
                                        <div class="col-md-3 col-xl-3">
                                            <x-product-card>
                                                <x-slot name="product_id">{{ $product->product->id }}</x-slot>
                                                <x-slot name="product_sku">{{ $product->product->sku }}</x-slot>
                                                <x-slot name="product_slug">{{ $product->product->slug }}</x-slot>
                                                <x-slot name="product_brand">{{ $product->product->brand->name }}</x-slot>
                                                <x-slot name="brand_logo">{{ $product->product->brand->logo }}</x-slot>
                                                <x-slot name="product_price">
                                                    {{formatPrice($product->product->product_price)}}
                                                </x-slot>
                                                <x-slot name="is_discount">{{ $product->product->is_discount }}</x-slot>
                                                <x-slot name="discount_price">{{ $product->product->discount_price }}</x-slot>
                                                <x-slot name="href">{{route('single.product',[$product->product->sku,$product->product->slug])}}</x-slot>
                                                <x-slot name="image">{{ filePath($product->product->image)}}</x-slot>
                                                <x-slot name="name">{{ $product->product->name }}</x-slot>
                                            </x-product-card>
                                        </div>

                                    @empty
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                            <img src="{{ asset('no-product-found.png') }}" alt="#no-product-found" class="rounded-lg">
                                        </div>
                                    @endforelse
                                </div>
                                <div class="ps-pagination">
                                    <ul class="pagination">
                                        {{ $products->links() }}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ps-section__left">
                    <div class="ps-block--vendor">
                        <div class="ps-block__thumbnail">
                            @if (empty($seller_store->shop_logo))
                                <img src="{{ asset('vendor-store.jpg') }}" alt="#{{ $seller_store->shop_name }}" class="rounded-lg">
                            @else
                                <img src="{{ filePath($seller_store->shop_logo) }}"
                                     alt="#{{ $seller_store->shop_name }}" class="rounded-lg">
                            @endif
                        </div>
                        <div class="ps-block__container">
                            <div class="ps-block__header">
                                <h4>{{ $seller_store->shop_name }}</h4>

                                @php

                                    $stars_count = App\Models\OrderProduct::where('shop_id', $seller_store->id)
                                                ->whereNotNull('review_star')
                                                ->select('review_star')
                                                ->get()
                                                ->toArray();

                                    $shop_stars_count = App\Models\OrderProduct::where('shop_id', $seller_store->id)
                                                ->whereNotNull('review_star')
                                                ->select('review_star')
                                                ->count();

                                    $rateArray =[];
                                    foreach ($stars_count as $star_count)
                                    {
                                        $rateArray[]= $star_count['review_star'];
                                    }
                                    $sum = array_sum($rateArray);

                                    $customer_count = App\Models\OrderProduct::where('shop_id', 1)
                                                        ->whereNotNull('review_star')
                                                        ->count();
                                   if ($customer_count > 0) {
                                        $result= round($sum/$customer_count);
                                    }else {
                                        $result= round($sum/1);
                                    }

                                @endphp

                                <div class="br-wrapper br-theme-fontawesome-stars">
                                    <div class="br-widget br-readonly">
                                        @for ($i = 0; $i < $result; $i++)
                                            <a href="javascript:void(0)" data-rating-value="1" data-rating-text="1"
                                               class="br-selected br-current"></a>
                                        @endfor
                                    </div>
                                </div>

                                <p>
                                    <strong>{{ ($result/5) * 100 }}% @translate(Positive)</strong>
                                    ({{ $shop_stars_count }}
                                    @translate(rating))
                                </p>
                            </div>
                            <span class="ps-block__divider"></span>
                            <div class="ps-block__content">
                                <p>
                                    <strong>{{ $seller_store->shop_name }}</strong>, {{ $seller_store->about }}.
                                </p>
                                <span class="ps-block__divider"></span>
                                <p>
                                    <strong>@translate(Address)</strong> {{ $seller_store->address }}
                                </p>

                                @if (!empty($seller_store->facebook))
                                    <figure>
                                        <figcaption>@translate(Follow us on social)</figcaption>
                                        <ul class="ps-list--social-color">
                                            <li>
                                                <a class="facebook" href="{{ $seller_store->facebook }}">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </figure>

                                @endif

                            </div>
                            <div class="ps-block__footer">
                                <p>@translate(Call us directly)
                                    <strong>{{ $seller_store->phone }}</strong>
                                </p>
                                <p>@translate(Or if you have any question)</p>
                                <a class="ps-btn ps-btn--fullwidth" href="mailto:{{ $seller_store->email }}">@translate(Contact Seller)</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- SHOP:END -->
@stop