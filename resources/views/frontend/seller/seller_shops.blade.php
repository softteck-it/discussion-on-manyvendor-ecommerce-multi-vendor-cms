@extends('frontend.master')


@section('title') @translate(Shops) @endsection

@section('content')

    <!-- breadcrumb:START -->
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ route('homepage') }}">@translate(Home)</a></li>
                <li>@translate(Shops)</li>
            </ul>
        </div>
    </div>
    <!-- breadcrumb:END -->

    <!-- SHOP LIST:START -->
    <div class="ps-whishlist">
        <div class="container">
            <div class="my-container text-center">
                <h1>@translate(All Shop)</h1>
            </div>
            <div class="ps-section__content t-mt-30">
                <!-- all brand list -->
                <div class="container">
                    <form class="ps-form--newsletter" action="" method="get">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <div class="ps-form__right">
                                    <div class="form-group--nest">
                                        <input class="form-control" name="search" value="{{Request::get('search')}}" type="text" id="myInput"
                                               placeholder="@translate(Search shop here)">
                                        <button class="ps-btn" type="submit">@translate(Search)</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="container" id="myShop">
                    <div class="row t-mt-30 no-gutters">
                        <!-- HERE GOES SHOPS -->
                        @forelse($seller_shops as $store)
                            @if($store->vendor != null)
                            <div class="col-md-2 col-xl-2">
                                <x-shop-card>
                                    <x-slot name="shop_id">{{ $store->vendor->id }}</x-slot>
                                    <x-slot name="shop_slug">{{ $store->vendor->slug }}</x-slot>
                                    <x-slot name="shop_name">{{ $store->vendor->shop_name }}</x-slot>
                                    <x-slot name="shop_logo">
                                        @if (empty($store->vendor->shop_logo))
                                            {{asset('vendor-store.jpg')}}
                                        @else
                                            {{ asset($store->vendor->shop_logo) }}
                                        @endif
                                    </x-slot>
                                    <x-slot name="shop_href">{{ route('vendor.shop',$store->vendor->slug) }}</x-slot>
                                </x-shop-card>
                            </div>
                            @endif
                        @empty
                            <div class="col-md-12">
                                <img src="{{ asset('shop-not-found.png') }}" class="img-fluid" alt="#shop-not-found">
                            </div>
                        @endforelse
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- SHOP LIST:END -->


@stop
