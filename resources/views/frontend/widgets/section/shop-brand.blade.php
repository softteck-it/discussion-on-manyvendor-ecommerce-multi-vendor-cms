
<div class="p-30 desktop-view brand-desktop-view-one">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="v3_header_title_area">
                    <span class="v3_header_title">@translate(Brands)</span>
                    <a href="{{ route('brands') }}" class="v3_all_link">@translate(view all)</a>
                </div>
            </div>
        </div>
        <div class="ps-section__content">
            <div class="ps-tab-root">
                <div class="ps-tabs">
                    <div class="ps-tabs">
                        <div class="ps-tab active mt-3">

                            <div class="row no-gutters">
                                @forelse (brands(16) as $brand)
                                <div class="col-md-2 col-xl-2">
                                    <x-brand-card>
                                        <x-slot name="brand_id">{{ $brand->id }}</x-slot>
                                        <x-slot name="brand_slug">{{ $brand->slug }}</x-slot>
                                        <x-slot name="brand_name">{{ $brand->name }}</x-slot>
                                        <x-slot name="brand_logo">
                                            @if (empty($brand->logo))
                                                {{asset('vendor-store.jpg')}}
                                            @else
                                                {{ filePath($brand->logo) }}
                                            @endif
                                        </x-slot>
                                        <x-slot name="brand_href">{{ route('brand.shop', $brand->slug) }}</x-slot>
                                    </x-brand-card>
                                </div>
                                @empty
                                <img src="{{ asset('No-Brand-Found.jpg') }}" class="img-fluid" alt="#no-brand-found">
                                @endforelse
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- Mobile View --}}
@includeWhen(true, 'frontend.mobile_widgets.brand')
{{-- Mobile View::END --}}