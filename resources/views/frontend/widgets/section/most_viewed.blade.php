

<div class="container">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="v3_header_title_area mb-2">
                <span class="v3_header_title">
                    @translate(Most Loved)
                </span>
            </div>
        </div>
    </div>

    <div class="row no-gutters">

        @forelse (default_frontend_product_catelog()->take(24) as $product)

        <div class="col-md-2 col-6">
            <x-product-card>
                <x-slot name="product_id">{{ $product->id }}</x-slot>
                <x-slot name="product_sku">{{ $product->sku }}</x-slot>
                <x-slot name="product_slug">{{ $product->slug }}</x-slot>
                <x-slot name="product_brand">{{ $product->brand->name }}</x-slot>
                <x-slot name="brand_logo">{{ $product->brand->logo }}</x-slot>
                @if (vendorActive())
                <x-slot name="product_price">
                    {{ formatPrice(brandProductPrice($product->sellers)->min())
                        == formatPrice(brandProductPrice($product->sellers)->max())
                        ? formatPrice(brandProductPrice($product->sellers)->min())
                        : formatPrice(brandProductPrice($product->sellers)->min()).
                        '-' .formatPrice(brandProductPrice($product->sellers)->max()) }}
                </x-slot>
                @else
                    <x-slot name="product_price">
                        {{formatPrice($product->product_price)}}
                    </x-slot>

                <x-slot name="is_discount">{{ $product->is_discount == null ? '0' : '1' }}</x-slot>
                <x-slot name="discount_price">{{ $product->discount_price }}</x-slot>

                @endif
                <x-slot name="href">{{route('single.product',[$product->sku,$product->slug])}}</x-slot>
                <x-slot name="image">{{ filePath($product->image)}}</x-slot>
                <x-slot name="name">{{ $product->name }}</x-slot>
            </x-product-card>
        </div>
            
        @empty
            
        @endforelse
       
    </div>

    <div class="row no-gutters" id="product_wrapper">
        {{-- HTML LOAD PRODUCTS --}}
    </div>

    <div class="row no-gutters d-none" id="product_skeleton">
        @for ($i = 0; $i < 12; $i++)
            <div class="col-md-2 col-6">
                <ul class="o-vertical-spacing o-vertical-spacing--l product-container">
                    <li class="o-media">
                        <div class="o-media__figure">
                            <span class="skeleton-box" style="width:180px;height:280px;"></span>
                        </div>
                    </li>
                </ul>
            </div>
        @endfor
    </div>

    <input type="hidden" value="{{ route('load.more.products') }}" id="load_more_products">
    <input type="hidden" value="1" id="load_more_btn_clicked">

    <div class="row mb-4 mt-4">
        <div class="col-lg-12">
            <div class="v3_load_more_btn text-center">
                <button class="btn btn-info rounded f-s-16 padding-4-16" onclick="LoadMoreProducts(12)">
                    <lord-icon
                        src="https://cdn.lordicon.com/pndvzexs.json"
                        trigger="loop"
                        delay="1000"
                        stroke="80"
                        colors="primary:#fff,secondary:#fff">
                    </lord-icon> 
                   @translate(Load More)
                </button>
            </div>
        </div>
    </div>
</div>
