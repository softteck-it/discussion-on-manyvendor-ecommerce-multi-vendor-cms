@if (bestSellingProduct()->count() > 0)
    <div class="container mb-2">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="v3_header_title_area mb-2">
                    <span class="v3_header_title">
                        @translate(Best Seller Products)
                    </span>
                    <a href="{{ route('all.product') }}" class="v3_all_link">@translate(view all)</a>
                </div>
            </div>
        </div>

        <div class="row no-gutters">

            @forelse (bestSellingProduct() as $product)
                <div class="col-md-2 col-6">
                    @if($product->main_product)
                    
                    {{-- <h6>{{ $product->main_product->name }}</h6> --}}
                    <x-product-card>

                        <x-slot name="product_id">{{ $product->main_product->id }}</x-slot>
                        <x-slot name="product_sku">{{ $product->main_product->sku }}</x-slot>
                        <x-slot name="product_slug">{{ $product->main_product->slug }}</x-slot>
                        <x-slot name="product_brand">{{ $product->main_product->brand->name }}</x-slot>
                        <x-slot name="brand_logo">{{ $product->main_product->brand->logo }}</x-slot>
                        @if (vendorActive())
                            <x-slot name="product_price">
                                {{ formatPrice(brandProductPrice($product->main_product->sellers)->min()) ==
                                formatPrice(brandProductPrice($product->main_product->sellers)->max())
                                    ? formatPrice(brandProductPrice($product->main_product->sellers)->min())
                                    : formatPrice(brandProductPrice($product->main_product->sellers)->min()) .
                                        '-' .
                                        formatPrice(brandProductPrice($product->main_product->sellers)->max()) }}
                            </x-slot>
                        @else
                            <x-slot name="product_price">
                                {{ formatPrice($product->main_product->product_price) }}
                            </x-slot>

                            <x-slot name="is_discount">{{ $product->main_product->is_discount == null ? '0' : '1' }}</x-slot>
                            <x-slot name="discount_price">{{ $product->main_product->discount_price }}</x-slot>
                        @endif
                        <x-slot name="href">{{ route('single.product', [$product->main_product->sku, $product->main_product->slug]) }}</x-slot>
                        <x-slot name="image">{{ filePath($product->main_product->image) }}</x-slot>
                        <x-slot name="name">{{ $product->main_product->name }}</x-slot>
                    </x-product-card>
                    @endif
                </div>

            @empty
            @endforelse

        </div>
    </div>
@else
@endif
