@if (recentView()->count() > 0)
    <div class="container mb-2">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="v3_header_title_area mb-2">
                    <span class="v3_header_title">
                        @translate(Recent View)
                    </span>
                    <a href="{{ route('all.product') }}" class="v3_all_link">@translate(view all)</a>
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            @forelse (recentView()->take(12) as $product)
                <div class="col-md-2 col-6">
                    <x-product-card>
                        <x-slot name="product_id">{{ $product->id }}</x-slot>
                        <x-slot name="product_sku">{{ $product->sku }}</x-slot>
                        <x-slot name="product_slug">{{ $product->slug }}</x-slot>
                        <x-slot name="product_brand">{{ $product->brand->name }}</x-slot>
                        <x-slot name="brand_logo">{{ $product->brand->logo }}</x-slot>
                        @if (vendorActive())
                            <x-slot name="product_price">
                                {{ formatPrice(brandProductPrice($product->sellers)->min()) ==
                                formatPrice(brandProductPrice($product->sellers)->max())
                                    ? formatPrice(brandProductPrice($product->sellers)->min())
                                    : formatPrice(brandProductPrice($product->sellers)->min()) .
                                        '-' .
                                        formatPrice(brandProductPrice($product->sellers)->max()) }}
                            </x-slot>
                        @else
                            <x-slot name="product_price">
                                {{ formatPrice($product->product_price) }}
                            </x-slot>

                            <x-slot name="is_discount">{{ $product->is_discount == null ? '0' : '1' }}</x-slot>
                            <x-slot name="discount_price">{{ $product->discount_price }}</x-slot>
                        @endif
                        <x-slot name="href">{{ route('single.product', [$product->sku, $product->slug]) }}</x-slot>
                        <x-slot name="image">{{ filePath($product->image) }}</x-slot>
                        <x-slot name="name">{{ $product->name }}</x-slot>
                    </x-product-card>
                </div>
            @empty
            @endforelse

        </div>
    </div>
@endif
