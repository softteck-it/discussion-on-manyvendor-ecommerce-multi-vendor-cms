@if (bestSeller()->count() > 0)
    @if (vendorActive())
        <div class="p-30 desktop-view">
            <div class="container my-container">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="v3_header_title_area">
                            <span class="v3_header_title text-white">@translate(Best Sellers)</span>
                            <a href="{{ route('vendor.shops') }}" class="v3_all_link text-white">@translate(view all)</a>
                        </div>
                    </div>
                </div>
                <div class="ps-section__content">
                    <div class="ps-block--categories-tabs ps-tab-root store_section">
                        <div class="ps-tabs">
                            <div class="ps-tabs">
                                <div class="ps-tab active p-0 mt-3">

                                    <div class="row no-gutters">
                                        @forelse (bestSeller() as $store)
                                            @if ($store->shop != null)
                                                <div class="col-md-2 col-xl-2">

                                                    <x-shop-card>
                                                        <x-slot name="shop_id">{{ $store->shop->id }}</x-slot>
                                                        <x-slot name="shop_slug">{{ $store->shop->slug }}</x-slot>
                                                        <x-slot name="shop_name">{{ $store->shop->shop_name }}</x-slot>
                                                        <x-slot name="shop_logo">
                                                            @if (empty($store->shop->shop_logo))
                                                                {{ asset('vendor-store.jpg') }}
                                                            @else
                                                                {{ asset($store->shop->shop_logo) }}
                                                            @endif
                                                        </x-slot>
                                                        <x-slot name="shop_href">
                                                            {{ route('vendor.shop', $store->shop->slug) }}</x-slot>
                                                    </x-shop-card>

                                                </div>
                                            @endif
                                        @empty
                                            <div class="col-md-12 col-sm-12">
                                                <img src="{{ asset('shop-not-found.png') }}" alt="">
                                            </div>
                                        @endforelse
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {{-- Mobile View --}}
        @includeWhen(true, 'frontend.mobile_widgets.shops')
        {{-- Mobile View::END --}}


    @endif
@else
@endif
