-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 01, 2023 at 09:33 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manyvendor_server`
--

-- --------------------------------------------------------

--
-- Table structure for table `addons`
--

CREATE TABLE `addons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unique_identifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `image` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_commissions`
--

CREATE TABLE `admin_commissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_product_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `confirm_by` bigint(20) UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `percentage` double(8,2) NOT NULL,
  `commission` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_accounts`
--

CREATE TABLE `affiliate_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `affiliation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_approved` tinyint(1) NOT NULL DEFAULT '0',
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `balance` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_paid_histories`
--

CREATE TABLE `affiliate_paid_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `is_cancelled` tinyint(1) NOT NULL DEFAULT '0',
  `payment_account` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paid_date` datetime DEFAULT NULL,
  `confirmed_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_payments_accounts`
--

CREATE TABLE `affiliate_payments_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Bank',
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `routing_number` int(11) DEFAULT NULL,
  `paypal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Paypal',
  `paypal_acc_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_acc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Stripe',
  `stripe_acc_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_acc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_card_holder_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payTm` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'payTm',
  `payTm_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bKash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'bKash',
  `bKash_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nagad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nagad',
  `nagad_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rocket` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Rocket',
  `rocket_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_product_links`
--

CREATE TABLE `affiliate_product_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `site` longtext COLLATE utf8mb4_unicode_ci,
  `link` longtext COLLATE utf8mb4_unicode_ci,
  `clicked` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_sell_histories`
--

CREATE TABLE `affiliate_sell_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `affiliation_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordered_product_id` bigint(20) UNSIGNED NOT NULL,
  `order_amount` double NOT NULL,
  `amount` double NOT NULL,
  `is_pending` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `is_requested` tinyint(1) NOT NULL DEFAULT '0',
  `meta_title` longtext COLLATE utf8mb4_unicode_ci,
  `meta_desc` longtext COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE `campaigns` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer` bigint(20) UNSIGNED NOT NULL,
  `start_from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_at` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active_for_seller` tinyint(1) NOT NULL DEFAULT '1',
  `active_for_customer` tinyint(1) NOT NULL DEFAULT '0',
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_desc` longtext COLLATE utf8mb4_unicode_ci,
  `meta_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_requested` tinyint(1) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_products`
--

CREATE TABLE `campaign_products` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_product_id` bigint(20) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `vpvs_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_product_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` bigint(20) UNSIGNED NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_popular` tinyint(1) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL DEFAULT '0',
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `is_requested` tinyint(1) NOT NULL DEFAULT '0',
  `parent_category_id` int(11) NOT NULL DEFAULT '0',
  `cat_group_id` bigint(20) UNSIGNED DEFAULT NULL,
  `meta_title` longtext COLLATE utf8mb4_unicode_ci,
  `meta_desc` longtext COLLATE utf8mb4_unicode_ci,
  `start_percentage` double DEFAULT NULL,
  `end_percentage` double DEFAULT NULL,
  `start_amount` double DEFAULT NULL,
  `end_amount` double DEFAULT NULL,
  `commission_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `primary_color` longtext COLLATE utf8mb4_unicode_ci,
  `secondary_color` longtext COLLATE utf8mb4_unicode_ci,
  `topbar_color` longtext COLLATE utf8mb4_unicode_ci,
  `topbar_text_color` longtext COLLATE utf8mb4_unicode_ci,
  `hover_color` longtext COLLATE utf8mb4_unicode_ci,
  `footer_color` longtext COLLATE utf8mb4_unicode_ci,
  `footer_text_color` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `commissions`
--

CREATE TABLE `commissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `type` enum('percentage','flat') COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_amount` double DEFAULT NULL,
  `end_amount` double DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `complains`
--

CREATE TABLE `complains` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_code` bigint(20) UNSIGNED NOT NULL,
  `complain_photos` longtext COLLATE utf8mb4_unicode_ci,
  `desc` longtext COLLATE utf8mb4_unicode_ci,
  `status` enum('solved','Not Solved','Untouched') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` double NOT NULL,
  `start_day` datetime NOT NULL,
  `end_day` datetime NOT NULL,
  `min_value` double DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `rate`, `start_day`, `end_day`, `min_value`, `is_published`, `created_at`, `updated_at`) VALUES
(1, 'demo', 50, '2020-09-01 19:33:00', '2020-09-24 19:33:00', 100, 1, '2020-09-24 07:34:07', '2020-09-24 07:34:07');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` double NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `align` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `code`, `symbol`, `rate`, `is_published`, `align`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Dollar', 'USD', '$', 1, 1, 1, 'Flag_of_the_United_States.png', NULL, '2021-01-02 11:08:46');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/avatar.jpg',
  `phn_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deliver_assigns`
--

CREATE TABLE `deliver_assigns` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deliver_user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivered` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pick` tinyint(1) NOT NULL DEFAULT '0',
  `pick_date` timestamp NULL DEFAULT NULL,
  `duration` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deliver_users`
--

CREATE TABLE `deliver_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permanent_address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `present_address` longtext COLLATE utf8mb4_unicode_ci,
  `phone_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pic` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `confirm` tinyint(1) NOT NULL DEFAULT '0',
  `confirm_by` bigint(20) UNSIGNED DEFAULT NULL,
  `confirm_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deliveymen_tracks`
--

CREATE TABLE `deliveymen_tracks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliverymen_id` bigint(20) UNSIGNED NOT NULL,
  `deliver_assign_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `district_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `district_name`, `created_at`, `updated_at`) VALUES
(1, 'Texas', '2020-09-24 06:35:49', '2020-09-24 06:35:49'),
(2, 'New Jersey', '2020-09-26 01:03:20', '2020-09-26 01:03:20');

-- --------------------------------------------------------

--
-- Table structure for table `ecom_campaign_products`
--

CREATE TABLE `ecom_campaign_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_carts`
--

CREATE TABLE `ecom_carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_stock_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_deliver_assigns`
--

CREATE TABLE `ecom_deliver_assigns` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deliver_user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `pick` tinyint(1) NOT NULL DEFAULT '0',
  `pick_date` datetime DEFAULT NULL,
  `duration` datetime DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'confirm',
  `delivered` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_deliver_tracks`
--

CREATE TABLE `ecom_deliver_tracks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliverymen_id` bigint(20) UNSIGNED NOT NULL,
  `deliver_assign_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_deliveymen_tracks`
--

CREATE TABLE `ecom_deliveymen_tracks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliverymen_id` bigint(20) UNSIGNED NOT NULL,
  `deliver_assign_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_orders`
--

CREATE TABLE `ecom_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  `area_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `logistic_id` bigint(20) UNSIGNED DEFAULT NULL,
  `logistic_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applied_coupon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` enum('cod','stripe','paypal') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_order_products`
--

CREATE TABLE `ecom_order_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `transaction_id` longtext COLLATE utf8mb4_unicode_ci,
  `booking_code` bigint(20) UNSIGNED NOT NULL,
  `order_number` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_stock_id` bigint(20) UNSIGNED DEFAULT NULL,
  `logistic_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` enum('cod','stripe','paypal') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','delivered','canceled','follow_up','processing','quality_check','product_dispatched','confirmed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` longtext COLLATE utf8mb4_unicode_ci,
  `review_star` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `commentedBy` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ecom_product_variant_stocks`
--

CREATE TABLE `ecom_product_variant_stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_variants_id` longtext COLLATE utf8mb4_unicode_ci,
  `product_variants` longtext COLLATE utf8mb4_unicode_ci,
  `quantity` int(11) NOT NULL,
  `extra_price` double NOT NULL DEFAULT '0',
  `alert_quantity` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `slug`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'super admin', 'super-admin', NULL, NULL, '2020-08-12 00:19:29', '2020-11-02 22:51:25'),
(2, 'customer', 'customer', NULL, NULL, '2020-08-12 04:06:36', '2020-08-12 04:06:36'),
(3, 'seller', 'seller', NULL, NULL, '2020-08-12 04:08:56', '2020-08-17 21:00:22'),
(5, 'Deliver section', 'deliver-section', NULL, NULL, '2020-12-27 00:44:34', '2020-12-27 00:44:34');

-- --------------------------------------------------------

--
-- Table structure for table `group_has_permissions`
--

CREATE TABLE `group_has_permissions` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `group_has_permissions`
--

INSERT INTO `group_has_permissions` (`group_id`, `permission_id`) VALUES
(2, 111),
(3, 106),
(3, 110),
(3, 113),
(1, 82),
(1, 83),
(1, 84),
(1, 85),
(1, 86),
(1, 87),
(1, 88),
(1, 89),
(1, 90),
(1, 91),
(1, 92),
(1, 93),
(1, 94),
(1, 95),
(1, 96),
(1, 97),
(1, 98),
(1, 99),
(1, 100),
(1, 101),
(1, 102),
(1, 103),
(1, 104),
(1, 105),
(1, 107),
(1, 108),
(1, 109),
(1, 112),
(1, 115),
(1, 116),
(1, 117),
(1, 106),
(1, 113),
(1, 114),
(5, 118);

-- --------------------------------------------------------

--
-- Table structure for table `infopages`
--

CREATE TABLE `infopages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `section` enum('top','bottom') COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `infopages`
--

INSERT INTO `infopages` (`id`, `section`, `icon`, `header`, `desc`, `page_id`, `created_at`, `updated_at`) VALUES
(1, 'bottom', 'fa fa-support', 'Support 24/7', 'Call us : (+09)001101001', 6, '2020-08-28 23:17:59', '2020-08-31 02:37:20'),
(2, 'bottom', 'fa fa-refresh', 'Secured Payment', 'Your payments are secured', 3, '2020-08-28 23:19:26', '2020-08-28 23:19:26'),
(3, 'bottom', 'fa fa-star', 'Product Quality', 'We ensure best products for you', 5, '2020-08-28 23:20:51', '2020-08-28 23:21:59'),
(4, 'bottom', 'fa fa-bullhorn', 'Special Offer', 'Exciting Offers', 5, '2020-08-28 23:30:24', '2020-08-28 23:30:24');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `code`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'en', 'English', 'Flag_of_the_United_States.png', NULL, NULL),
(2, 'ge', 'Germany', 'Flag_of_Germany.png', '2021-01-03 11:22:37', '2021-01-03 11:22:37'),
(3, 'fr', 'French', 'Flag_of_France.png', '2021-08-23 12:04:50', '2021-08-23 12:04:50'),
(4, 'tu', 'Russian', 'Flag_of_Russia.png', '2021-08-29 04:15:19', '2021-08-29 04:15:19'),
(5, 'bn', 'Bangali', 'Flag_of_Bangladesh.png', '2021-09-22 07:11:37', '2021-09-22 07:11:37'),
(6, 'hi', 'India', 'Flag_of_India.png', '2021-09-23 11:35:42', '2021-09-23 11:35:42'),
(7, 'tr', 'Turkey', 'Flag_of_Turkey.png', '2021-09-23 11:47:44', '2021-09-23 11:47:44'),
(8, 'ml', 'Malaysia', 'Flag_of_Malaysia.png', '2021-09-25 04:20:53', '2021-09-25 04:20:53'),
(9, 'jp', 'Japan', 'Flag_of_Japan.png', '2021-10-02 05:26:05', '2021-10-02 05:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `logistics`
--

CREATE TABLE `logistics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logistic_areas`
--

CREATE TABLE `logistic_areas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logistic_id` bigint(20) UNSIGNED NOT NULL,
  `division_id` bigint(20) UNSIGNED NOT NULL,
  `area_id` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` double NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_03_16_005237_create_permissions_table', 1),
(9, '2019_03_16_005538_create_user_has_permissions_table', 1),
(10, '2019_03_16_005634_create_groups_table', 1),
(11, '2019_03_16_005759_create_group_has_permissions_table', 1),
(12, '2019_03_16_005834_create_user_has_groups_table', 1),
(13, '2019_08_19_000000_create_failed_jobs_table', 1),
(14, '2020_05_21_093740_create_settings_table', 1),
(15, '2020_05_21_093839_create_categories_table', 1),
(16, '2020_05_21_093908_create_currencies_table', 1),
(17, '2020_05_21_093935_create_languages_table', 1),
(18, '2020_05_21_203324_create_pages_table', 1),
(19, '2020_05_21_203341_create_page_contents_table', 1),
(20, '2020_06_12_164431_create_modules_table', 1),
(21, '2020_06_12_164655_create_module_has_permissions_table', 1),
(22, '2020_06_17_075327_create_brands_table', 1),
(23, '2020_06_18_152443_create_commissions_table', 1),
(24, '2020_06_18_205802_create_vendors_table', 1),
(25, '2020_06_19_061010_create_category_groups_table', 1),
(26, '2020_06_20_063516_create_section_settings_table', 1),
(27, '2020_06_23_080421_create_variants_table', 1),
(28, '2020_06_24_044443_create_products_table', 1),
(29, '2020_06_24_045956_create_product_images_table', 1),
(30, '2020_06_24_050102_create_product_variants_table', 1),
(31, '2020_06_27_094308_create_customers_table', 1),
(32, '2020_06_27_132906_create_vendor_products_table', 1),
(33, '2020_07_04_082306_create_carts_table', 1),
(34, '2020_07_06_052133_create_promotions_table', 1),
(35, '2020_07_06_130948_create_wishlists_table', 1),
(36, '2020_07_08_054549_create_districts_table', 1),
(37, '2020_07_08_054723_create_thanas_table', 1),
(38, '2020_07_08_071829_create_logistics_table', 1),
(39, '2020_07_08_091228_create_logistic_areas_table', 1),
(40, '2020_07_09_084030_create_vendor_product_variant_stocks_table', 1),
(41, '2020_07_09_100615_create_campaigns_table', 1),
(42, '2020_07_09_114210_create_coupons_table', 1),
(43, '2020_07_13_065921_create_campaign_products_table', 1),
(44, '2020_07_23_120041_create_orders_table', 1),
(45, '2020_07_23_121543_create_order_products_table', 1),
(46, '2020_07_29_094601_create_complains_table', 1),
(47, '2020_08_07_052102_create_page_groups_table', 1),
(48, '2020_08_10_104715_create_admin_commissions_table', 1),
(49, '2020_08_13_092807_create_infopages_table', 1),
(50, '2020_08_18_032558_create_seller_earnings_table', 1),
(51, '2020_08_18_071516_create_seller_accounts_table', 1),
(52, '2020_08_18_072008_create_payments_table', 1),
(53, '2020_08_20_083105_create_ecom_product_variant_stocks_table', 1),
(54, '2020_08_23_034334_create_ecom_campaign_products_table', 1),
(55, '2020_08_23_052929_create_ecom_carts_table', 1),
(56, '2020_08_24_032931_create_ecom_orders_table', 1),
(57, '2020_08_24_033032_create_ecom_order_products_table', 1),
(58, '2020_11_03_064315_create_verify_users_table', 1),
(64, '2020_12_27_061137_create_deliver_assigns_table', 2),
(65, '2020_12_28_042652_create_deliver_users_table', 2),
(67, '2020_12_29_052949_create_deliveymen_tracks_table', 3),
(68, '2020_12_30_113834_create_ecom_deliver_assigns_table', 4),
(69, '2020_12_30_113912_create_ecom_deliver_tracks_table', 4),
(70, '2020_12_30_114918_create_ecom_deliveymen_tracks_table', 5),
(71, '2021_06_23_112219_create_colors_table', 6),
(72, '2021_10_17_175826_create_jobs_table', 7),
(73, '2022_03_28_172930_create_product_views_table', 8),
(75, '2022_04_02_152116_create_affiliate_product_links_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `created_at`, `updated_at`) VALUES
(6, 'backend Module', '2020-08-12 03:18:02', '2020-08-17 20:59:19'),
(7, 'Seller Panel', '2020-08-12 03:36:16', '2020-08-12 03:36:16'),
(8, 'front End', '2020-08-12 03:36:27', '2020-08-12 03:36:27'),
(9, 'order managment', '2020-08-17 20:59:36', '2020-08-17 20:59:36'),
(10, 'Payment manage', '2020-08-18 03:19:46', '2020-08-18 03:19:46'),
(11, 'delivermen', '2020-12-27 00:44:04', '2020-12-27 00:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `module_has_permissions`
--

CREATE TABLE `module_has_permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `module_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_has_permissions`
--

INSERT INTO `module_has_permissions` (`id`, `permission_id`, `module_id`, `created_at`, `updated_at`) VALUES
(164, 111, 8, '2020-08-12 03:36:27', '2020-08-12 03:36:27'),
(196, 110, 7, '2020-08-17 20:58:49', '2020-08-17 20:58:49'),
(225, 106, 9, '2020-08-17 20:59:36', '2020-08-17 20:59:36'),
(226, 113, 9, '2020-08-17 20:59:37', '2020-08-17 20:59:37'),
(227, 114, 10, '2020-08-18 03:19:46', '2020-08-18 03:19:46'),
(228, 82, 6, '2020-08-25 04:35:21', '2020-08-25 04:35:21'),
(229, 83, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(230, 84, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(231, 85, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(232, 86, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(233, 87, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(234, 88, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(235, 89, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(236, 90, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(237, 91, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(238, 92, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(239, 93, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(240, 94, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(241, 95, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(242, 96, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(243, 97, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(244, 98, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(245, 99, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(246, 100, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(247, 101, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(248, 102, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(249, 103, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(250, 104, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(251, 105, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(252, 107, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(253, 108, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(254, 109, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(255, 112, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(256, 115, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(257, 116, 6, '2020-08-25 04:35:22', '2020-08-25 04:35:22'),
(258, 117, 6, NULL, NULL),
(259, 118, 11, '2020-12-27 00:44:04', '2020-12-27 00:44:04'),
(260, 119, 6, '2020-12-27 00:44:04', '2020-12-27 00:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('336bf52fbef53630992dd542370daf6fac3a269b68783c91f07677a3eb2631f0ec0f4cd2a8b0c52a', 12, 1, 'Everything seems impossible before it is done', '[]', 1, '2020-11-18 23:06:07', '2020-11-18 23:06:07', '2021-11-19 05:06:07'),
('36f9d3081c1f365ba110a98d9e43fa9e07e7b75f595b8db29796972dff9441c909a64b050968bb6d', 12, 1, 'Everything seems impossible before it is done', '[]', 0, '2020-11-16 09:48:59', '2020-11-16 09:48:59', '2021-11-16 04:48:59'),
('58a4fe34c8c35b8d5fe981fe6ce01bcbd8f024b523b697c54e825042e0bfc53934b90e03e530ed58', 12, 1, 'Everything seems impossible before it is done', '[]', 0, '2020-11-18 23:38:20', '2020-11-18 23:38:20', '2021-11-19 05:38:20'),
('6276af7f0e9287d9163adc07341a69fe72d7baee35d797be198a68f9eee3f90f205fe9caca51414c', 12, 1, 'Everything seems impossible before it is done', '[]', 1, '2020-11-18 23:25:33', '2020-11-18 23:25:33', '2021-11-19 05:25:33'),
('77c366b7b3f661b57b5354cff84f28e037dfecf220df406039cc4d402aaf2e86b6ef6e6b37ef79ea', 12, 1, 'Everything seems impossible before it is done', '[]', 0, '2020-11-18 23:16:37', '2020-11-18 23:16:37', '2021-11-19 05:16:37'),
('9c9094856593a5ab5e2c117fa6b3493f9c019815ea72bf479a806d8238842f2a0d8a12016550ad8a', 12, 1, 'Everything seems impossible before it is done', '[]', 0, '2020-11-15 14:43:23', '2020-11-15 14:43:23', '2021-11-15 09:43:23'),
('9e339064abc3cd77f223587c4e37f4b42c9c4a2acfc1353445719ad17e39cdcf23c839a6849a0585', 12, 1, 'Everything seems impossible before it is done', '[]', 0, '2020-11-16 11:59:16', '2020-11-16 11:59:16', '2021-11-16 06:59:16'),
('9e73c1dee9a2a4ac9f44f3a67cf75cee426e87a18449c85dba4bae346e628d62a40a18e8a9d5c844', 12, 1, 'Everything seems impossible before it is done', '[]', 0, '2020-11-16 11:58:13', '2020-11-16 11:58:13', '2021-11-16 06:58:13'),
('c3a88fc2c1dda2b51f2e521de093d9f49fb190e367192401ee72b6d8728a3212cace69125a0d7225', 12, 1, 'Everything seems impossible before it is done', '[]', 1, '2020-11-18 23:04:05', '2020-11-18 23:04:05', '2021-11-19 05:04:05');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Manyvendor Personal Access Client', 'o6ZnLH0Y5WBSOiPP7ecaGvVVrBF3l8ZL3zS5LAmW', NULL, 'http://localhost', 1, 0, 0, '2020-11-04 05:15:45', '2020-11-04 05:15:45'),
(2, NULL, 'Manyvendor Password Grant Client', '9BhQhUljGSbKrKXRLqJFpZfxeqaUWQiK1zPnYIu7', 'users', 'http://localhost', 0, 1, 0, '2020-11-04 05:15:45', '2020-11-04 05:15:45');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-11-04 05:15:45', '2020-11-04 05:15:45');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `division_id` int(11) NOT NULL,
  `area_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `logistic_id` bigint(20) UNSIGNED DEFAULT NULL,
  `logistic_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applied_coupon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` enum('cod','stripe','paypal','paytm','ssl-commerz') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `booking_code` bigint(20) UNSIGNED NOT NULL,
  `order_number` bigint(20) UNSIGNED NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) UNSIGNED NOT NULL,
  `logistic_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` enum('cod','stripe','paypal','paytm','ssl-commerz') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','delivered','canceled','follow_up','processing','quality_check','product_dispatched','confirmed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` longtext COLLATE utf8mb4_unicode_ci,
  `review_star` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `commentedBy` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `page_group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `active`, `created_at`, `updated_at`, `page_group_id`) VALUES
(1, 'How It Works', 'how-it-works', 1, '2020-08-28 22:57:18', '2021-01-03 10:53:27', 6),
(2, 'Reviews', 'reviews', 1, '2020-08-28 23:00:03', '2021-01-03 10:54:24', 6),
(3, 'Privacy Policy', 'privacy-policy', 1, '2020-08-28 23:02:55', '2021-01-03 10:54:34', 5),
(4, 'Cookie Policy', 'cookie-policy', 1, '2020-08-28 23:04:00', '2021-01-03 10:54:41', 5),
(5, 'Purchasing Policy', 'purchasing-policy', 1, '2020-08-28 23:04:33', '2021-01-03 10:54:56', 5),
(6, 'About Us', 'about-us', 1, '2020-08-28 23:13:59', '2021-01-03 10:55:04', 6),
(7, 'Affiliate Marketing', 'affiliate-marketing', 1, '2020-08-28 23:15:41', '2021-01-03 10:55:10', 5),
(8, 'how', 'how', 0, '2021-01-02 10:38:19', '2021-01-03 10:53:16', 5),
(9, 'Site Map', 'site-map', 1, '2021-01-03 10:56:45', '2021-01-03 10:56:45', 7),
(10, 'Place Order', 'place-order', 1, '2021-01-03 10:57:27', '2021-01-03 10:57:27', 7),
(11, 'Location', 'location', 1, '2021-01-03 10:58:24', '2021-01-03 10:58:24', 8),
(12, 'Address', 'address', 1, '2021-01-03 10:58:37', '2021-01-03 10:58:37', 8),
(13, 'Location 2', 'location-2', 1, '2021-01-03 10:58:52', '2021-01-03 10:58:52', 8),
(14, 'Page 1', 'page-1', 1, '2021-01-03 11:07:55', '2021-01-03 11:07:55', 6),
(15, 'Page 2', 'page-2', 1, '2021-01-03 11:08:14', '2021-01-03 11:08:14', 7),
(16, 'Page 3', 'page-3', 1, '2021-01-03 11:08:24', '2021-01-03 11:08:24', 7),
(17, 'Page 4', 'page-4', 1, '2021-01-03 11:08:37', '2021-01-03 11:08:37', 8);

-- --------------------------------------------------------

--
-- Table structure for table `page_contents`
--

CREATE TABLE `page_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` bigint(20) UNSIGNED NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `meta_data` longtext COLLATE utf8mb4_unicode_ci,
  `meta_desc` longtext COLLATE utf8mb4_unicode_ci,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_contents`
--

INSERT INTO `page_contents` (`id`, `title`, `page_id`, `body`, `active`, `meta_data`, `meta_desc`, `sorting`, `created_at`, `updated_at`) VALUES
(9, 'How it works', 1, '<div style=\"text-align: center;\"><font face=\"Open Sans, Arial, sans-serif\"><span style=\"font-size: 14px;\"><b>dssadasdsadasdsadhasjhdasjdhgashkjasdv haskdhjahs adgvhsdhfaskjdhasdgasjkhdgshadghajksdashdjkasdgkjashdgkashdfgsjdfhasdgaskjfhgjsdfgkjasddhfeuioq ruqwojdhjkashfklashdbhashflkasjhdbashdkjashdajsdh gasdfasaksjdhasdfhkjashdgjasdha</b></span></font><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-weight: bolder;\">dssadasdsadasdsadhasjhdasjdhgashkjasdv haskdhjahs adgvhsdhfaskjdhasdgasjkhdgshadghajksdashdjkasdgkjashdgkashdfgsjdfhasdgaskjfhgjsdfgkjasddhfeuioq ruqwojdhjkashfklashdbhashflkasjhdbashdkjashdajsdh gasdfasaksjdhasdfhkjashdgjasdha</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-weight: bolder;\">dssadasdsadasdsadhasjhdasjdhgashkjasdv haskdhjahs adgvhsdhfaskjdhasdgasjkhdgshadghajksdashdjkasdgkjashdgkashdfgsjdfhasdgaskjfhgjsdfgkjasddhfeuioq ruqwojdhjkashfklashdbhashflkasjhdbashdkjashdajsdh gasdfasaksjdhasdfhkjashdgjasdha</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-weight: bolder;\">dssadasdsadasdsadhasjhdasjdhgashkjasdv haskdhjahs adgvhsdhfaskjdhasdgasjkhdgshadghajksdashdjkasdgkjashdgkashdfgsjdfhasdgaskjfhgjsdfgkjasddhfeuioq ruqwojdhjkashfklashdbhashflkasjhdbashdkjashdajsdh gasdfasaksjdhasdfhkjashdgjasdha</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-weight: bolder;\">dssadasdsadasdsadhasjhdasjdhgashkjasdv haskdhjahs adgvhsdhfaskjdhasdgasjkhdgshadghajksdashdjkasdgkjashdgkashdfgsjdfhasdgaskjfhgjsdfgkjasddhfeuioq ruqwojdhjkashfklashdbhashflkasjhdbashdkjashdajsdh gasdfasaksjdhasdfhkjashdgjasdha</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-weight: bolder;\">dssadasdsadasdsadhasjhdasjdhgashkjasdv haskdhjahs adgvhsdhfaskjdhasdgasjkhdgshadghajksdashdjkasdgkjashdgkashdfgsjdfhasdgaskjfhgjsdfgkjasddhfeuioq ruqwojdhjkashfklashdbhashflkasjhdbashdkjashdajsdh gasdfasaksjdhasdfhkjashdgjasdha</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-weight: bolder;\">dssadasdsadasdsadhasjhdasjdhgashkjasdv haskdhjahs adgvhsdhfaskjdhasdgasjkhdgshadghajksdashdjkasdgkjashdgkashdfgsjdfhasdgaskjfhgjsdfgkjasddhfeuioq ruqwojdhjkashfklashdbhashflkasjhdbashdkjashdajsdh gasdfasaksjdhasdfhkjashdgjasdha</span></div><p><div style=\"margin: 0px 28.7969px 0px 14.3906px; padding: 0px; width: 436.797px; text-align: left; float: right; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\"></div></p>', 1, NULL, NULL, 0, '2021-09-28 10:29:55', '2021-09-28 10:29:55');

-- --------------------------------------------------------

--
-- Table structure for table `page_groups`
--

CREATE TABLE `page_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_groups`
--

INSERT INTO `page_groups` (`id`, `name`, `is_published`, `created_at`, `updated_at`) VALUES
(5, 'Our Services', 1, '2021-01-02 10:37:55', '2021-01-03 10:50:48'),
(6, 'Information', 1, '2021-01-03 10:51:07', '2021-01-03 10:51:07'),
(7, 'About Site', 1, '2021-01-03 10:56:12', '2021-01-03 10:56:12'),
(8, 'Contact Us', 1, '2021-01-03 10:56:29', '2021-01-03 10:56:29');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `current_balance` double DEFAULT NULL,
  `process` enum('Bank','Paypal','Stripe') COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Request','Confirm') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_change_date` datetime DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(82, 'dashboard', 'dashboard', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">dashboard</pre>', NULL, '2020-08-12 03:10:28', '2020-08-12 03:10:28'),
(83, 'user management', 'user-management', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">user-management</pre>', NULL, '2020-08-12 03:10:35', '2020-08-12 03:10:35'),
(84, 'user setup', 'user-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">user-setup</pre>', NULL, '2020-08-12 03:10:40', '2020-08-12 03:10:40'),
(85, 'group setup', 'group-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">group-setup</pre>', NULL, '2020-08-12 03:10:45', '2020-08-12 03:10:45'),
(86, 'manage permissions', 'permissions-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">permissions-manage</pre>', NULL, '2020-08-12 03:10:51', '2020-08-12 03:10:51'),
(87, 'mail configuration', 'mail-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">mail-setup</pre>', NULL, '2020-08-12 03:10:56', '2020-08-12 03:10:56'),
(88, 'site settings', 'site-setting', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">site-setting</pre>', NULL, '2020-08-12 03:11:01', '2020-08-12 03:11:01'),
(89, 'language setup', 'language-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">language-setup</pre>', NULL, '2020-08-12 03:11:07', '2020-08-12 03:11:07'),
(90, 'currency setup', 'currency-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">currency-setup</pre>', NULL, '2020-08-12 03:11:12', '2020-08-12 03:11:12'),
(91, 'manage pages', 'pages-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">pages-manage</pre>', NULL, '2020-08-12 03:11:40', '2020-08-12 03:11:40'),
(92, 'category management', 'category-management', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">category-management</pre>', NULL, '2020-08-12 03:11:50', '2020-08-12 03:11:50'),
(93, 'commission management', 'commission-management', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">commission-management</pre>', NULL, '2020-08-12 03:11:55', '2020-08-12 03:11:55'),
(94, 'section settings', 'section-setting', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">section-setting</pre>', NULL, '2020-08-12 03:12:26', '2020-08-12 03:12:26'),
(95, 'additional setting', 'additional-setting', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">additional-setting</pre>', NULL, '2020-08-12 03:12:33', '2020-08-12 03:12:33'),
(96, 'manage product variant', 'product-variant-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">product-variant-manage</pre>', NULL, '2020-08-12 03:12:38', '2020-08-12 03:12:38'),
(97, 'manage product', 'product-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">product-manage</pre>', NULL, '2020-08-12 03:12:44', '2020-08-12 03:12:44'),
(98, 'ecommerce setting', 'ecommerce-setting', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">ecommerce-setting</pre>', NULL, '2020-08-12 03:12:49', '2020-08-12 03:12:49'),
(99, 'manage brand', 'brand-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">brand-manage</pre>', NULL, '2020-08-12 03:12:53', '2020-08-12 03:12:53'),
(100, 'manage campaign', 'campaign-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">campaign-manage</pre>', NULL, '2020-08-12 03:12:58', '2020-08-12 03:12:58'),
(101, 'payment method setup', 'payment-method-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">payment-method-setup</pre>', NULL, '2020-08-12 03:13:26', '2020-08-12 03:13:26'),
(102, 'promotions banner setup', 'promotions-banner-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">promotions-banner-setup</pre>', NULL, '2020-08-12 03:13:32', '2020-08-12 03:13:32'),
(103, 'main slider', 'main-slider-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">main-slider-setup</pre>', NULL, '2020-08-12 03:13:37', '2020-08-12 03:13:37'),
(104, 'shipping setup', 'shipping-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">shipping-setup</pre>', NULL, '2020-08-12 03:13:44', '2020-08-12 03:13:44'),
(105, 'coupon setup', 'coupon-setup', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">coupon-setup</pre>', NULL, '2020-08-12 03:13:50', '2020-08-12 03:13:50'),
(106, 'order manage', 'order-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">order-manage</pre>', NULL, '2020-08-12 03:13:56', '2020-08-12 03:13:56'),
(107, 'fullfillment', 'fullfill-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">fullfill-manage</pre>', NULL, '2020-08-12 03:14:01', '2020-08-12 03:14:01'),
(108, 'manage complain', 'complain-manage', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">complain-manage</pre>', NULL, '2020-08-12 03:14:07', '2020-08-12 03:14:07'),
(109, 'admin earning', 'admin-earning', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\">admin-earning</pre>', NULL, '2020-08-12 03:14:13', '2020-08-12 03:14:13'),
(110, 'seller', 'seller', NULL, NULL, '2020-08-12 03:35:48', '2020-08-12 03:35:48'),
(111, 'customer', 'customer', NULL, NULL, '2020-08-12 03:35:57', '2020-08-12 03:35:57'),
(112, 'seller management', 'seller-management', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\"><span style=\"color:#6a8759;background-color:#232525;\">seller.management</span></pre>', NULL, '2020-08-12 05:05:42', '2020-08-12 05:05:42'),
(113, 'order-modify', 'order-modify', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\"><span style=\"color:#6a8759;background-color:#232525;\">order-modify</span></pre>', NULL, '2020-08-17 20:57:54', '2020-08-17 20:57:54'),
(114, 'seller-payment', 'seller-payment', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\"><span style=\"color:#6a8759;background-color:#232525;\">seller-payment</span></pre>', NULL, '2020-08-18 03:18:53', '2020-08-18 03:18:53'),
(115, 'switch mode', 'app-active', '<pre style=\"background-color:#2b2b2b;color:#a9b7c6;font-family:\'JetBrains Mono\',monospace;font-size:9.8pt;\"><span style=\"color:#6a8759;background-color:#232525;\">app-active</span></pre>', NULL, '2020-08-25 04:35:05', '2020-08-25 04:35:05'),
(116, 'addons-manager', 'addons-manager', NULL, NULL, NULL, NULL),
(117, 'affiliate-management', 'affiliate-management', NULL, NULL, NULL, NULL),
(118, 'deliver', 'deliver', '<p>this deliver</p>', NULL, '2020-12-27 00:43:46', '2020-12-27 00:43:46'),
(119, 'deliver-management', 'deliver-management', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` bigint(20) DEFAULT NULL,
  `short_desc` longtext COLLATE utf8mb4_unicode_ci,
  `big_desc` longtext COLLATE utf8mb4_unicode_ci,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `category_group_id` bigint(20) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `digital_product` tinyint(1) NOT NULL DEFAULT '0',
  `main_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` enum('youtube','vimeo') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` longtext COLLATE utf8mb4_unicode_ci,
  `meta_desc` longtext COLLATE utf8mb4_unicode_ci,
  `meta_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` longtext COLLATE utf8mb4_unicode_ci,
  `is_request` tinyint(1) NOT NULL DEFAULT '0',
  `have_variant` tinyint(1) NOT NULL DEFAULT '0',
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `tax` double NOT NULL DEFAULT '0',
  `product_price` double DEFAULT NULL,
  `purchase_price` double DEFAULT NULL,
  `is_discount` tinyint(1) DEFAULT NULL,
  `discount_type` enum('flat','per') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_price` double DEFAULT NULL,
  `discount_percentage` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile_desc` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variant_id` bigint(20) UNSIGNED DEFAULT NULL,
  `unit` longtext COLLATE utf8mb4_unicode_ci,
  `is_published` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variant_stocks`
--

CREATE TABLE `product_variant_stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_variants_id` longtext COLLATE utf8mb4_unicode_ci,
  `product_variants` longtext COLLATE utf8mb4_unicode_ci,
  `quantity` int(11) NOT NULL,
  `extra_price` double NOT NULL DEFAULT '0',
  `alert_quantity` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_views`
--

CREATE TABLE `product_views` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `category_group_id` bigint(20) UNSIGNED DEFAULT NULL,
  `total_count` longtext COLLATE utf8mb4_unicode_ci,
  `total_search_count` int(11) NOT NULL DEFAULT '0',
  `ipAddress` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE `promotions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` longtext COLLATE utf8mb4_unicode_ci,
  `is_published` tinyint(1) NOT NULL,
  `type` enum('header','category','section','mainSlider','popup','sidepopup') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `section_settings`
--

CREATE TABLE `section_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `active` double NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blade_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `section_settings`
--

INSERT INTO `section_settings` (`id`, `active`, `sort`, `name`, `blade_name`, `image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Hero Section', 'main-banner', 'images/section/main-banner.png', NULL, NULL, '2020-08-08 03:38:49'),
(2, 0, 2, 'Top Categories Of The Month', 'search-trending', 'images/section/search-trending.png', NULL, NULL, '2022-03-29 07:54:23'),
(3, 1, 3, 'Campaigns', 'deal-day', 'images/section/deal-day.png', NULL, NULL, '2021-09-25 11:49:27'),
(4, 1, 4, 'Brands', 'shop-brand', 'images/section/shop-brand.png', NULL, NULL, '2021-09-25 11:49:29'),
(5, 1, 6, 'Shops', 'shop-store', 'images/section/shop-store.png', NULL, NULL, '2021-09-25 11:39:32'),
(6, 1, 5, 'Promotions', 'promotional-banner', 'images/section/promotional-banner.png', NULL, NULL, '2021-09-25 11:38:32'),
(7, 0, 7, 'Categories', 'top-categories', 'images/section/top-categories.png', NULL, NULL, '2022-03-29 07:56:11'),
(8, 0, 8, 'Popular Categories', 'category-promotional', 'images/section/category-promotional.png', NULL, NULL, '2022-03-29 07:56:13'),
(9, 1, 9, 'Best Seller Products', 'best-seller-products', 'images/section/best-seller-product.png', NULL, NULL, '2023-01-11 09:29:34'),
(10, 1, 10, 'Recent View Products', 'recent-view-products', 'images/section/recent-view-section.png', NULL, NULL, '2023-01-11 09:29:35'),
(11, 1, 10, 'Best Sellers', 'best-sellers', 'images/section/best-seller.png', NULL, NULL, '2023-01-11 09:29:37'),
(12, 1, 10, 'Most Searched Products', 'most-searched-products', 'images/section/most-searched-products.png', NULL, NULL, '2023-01-11 09:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `seller_accounts`
--

CREATE TABLE `seller_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Bank',
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `routing_number` int(11) DEFAULT NULL,
  `paypal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Paypal',
  `paypal_acc_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_acc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Stripe',
  `stripe_acc_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_acc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_card_holder_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seller_earnings`
--

CREATE TABLE `seller_earnings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_product_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_product_stock_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_product_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `commission_pay` double DEFAULT NULL,
  `get_amount` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'default_currencies', '1', NULL, NULL),
(2, 'type_logo', 'uploads/site/8gwdrfmA3NPo2ieEwczm4WQvEA1hjLjSE7FfS6Gd.png', NULL, '2022-04-03 08:09:03'),
(3, 'type_name', 'Manyvendor', NULL, '2023-01-11 09:28:49'),
(4, 'type_footer', 'Manyvendor', NULL, '2020-09-22 06:05:56'),
(5, 'type_mail', 'admin@mail.com', NULL, '2023-01-11 09:28:49'),
(6, 'type_address', 'Dhaka, Bangladesh', NULL, '2023-01-11 09:28:49'),
(7, 'type_fb', 'https://www.facebook.com/developerjahangir', NULL, '2023-01-11 09:28:49'),
(8, 'type_tw', 'twitter.com/core', NULL, '2023-01-11 09:28:49'),
(9, 'type_number', '01767275819', NULL, '2023-01-11 09:28:49'),
(10, 'type_google', 'insm.com/core', NULL, '2023-01-11 09:28:49'),
(11, 'footer_logo', NULL, NULL, '2020-08-06 00:07:09'),
(12, 'favicon_icon', 'uploads/site/WU01pUNSdsLDKpHitn3iNRutY1JbXgaY7Po6H1OS.png', NULL, '2020-09-22 06:05:56'),
(13, 'seller', 'enable', NULL, NULL),
(14, 'primary_color', NULL, NULL, '2020-08-06 00:44:26'),
(15, 'secondary_color', NULL, NULL, '2020-08-06 00:44:26'),
(16, 'seller_mode', 'request', NULL, '2020-12-29 05:07:50'),
(17, 'verification', 'on', NULL, '2020-12-29 05:07:50'),
(18, 'login_modal', 'on', NULL, '2020-08-06 00:44:27'),
(19, 'payment_logo', 'uploads/logo/8iN3FR7aoQlaKtTUpZG4txLVG3vF7q5OXtNqWQ6p.png', NULL, '2020-08-09 23:31:44'),
(20, 'type_ios', 'https://play.google.com/store/apps/details?id=com.softechit.many_vendor_ecommerce_app', NULL, '2020-10-22 09:47:51'),
(21, 'type_appstore', 'uploads/site/ItQJHlwNBesLo098tuYq5lFFKtz9XKoTT0SvYzW8.png', NULL, '2020-10-22 09:55:18'),
(22, 'type_playstore', 'uploads/site/HrxX6heoPShhSqt2DFyxYeu3We2BU2Fz7Fve40cY.png', NULL, '2020-10-22 09:55:18'),
(23, 'type_android', 'https://play.google.com/store/apps/details?id=com.multivendor.many_vendor_app', NULL, '2020-10-22 09:47:51'),
(45, 'affiliate_commission', NULL, '2020-11-03 03:34:22', '2020-11-03 03:34:22'),
(46, 'affiliate_min_withdrawal', NULL, '2020-11-03 03:34:22', '2020-11-03 03:34:22'),
(47, 'affiliate_cookie_limit', NULL, '2020-11-03 03:34:22', '2020-11-03 03:34:22'),
(48, 'affiliate_payment', NULL, '2020-11-03 03:34:22', '2020-11-03 03:34:22'),
(49, 'affiliate_commission', NULL, '2020-11-04 00:54:59', '2020-11-04 00:54:59'),
(50, 'affiliate_min_withdrawal', NULL, '2020-11-04 00:54:59', '2020-11-04 00:54:59'),
(51, 'affiliate_cookie_limit', NULL, '2020-11-04 00:54:59', '2020-11-04 00:54:59'),
(52, 'affiliate_payment', NULL, '2020-11-04 00:54:59', '2020-11-04 00:54:59'),
(53, 'bank_qrcode', NULL, '2020-11-04 00:54:59', '2020-11-04 00:54:59'),
(54, 'affiliate_commission', NULL, '2021-10-28 08:08:32', '2021-10-28 08:08:32'),
(55, 'affiliate_min_withdrawal', NULL, '2021-10-28 08:08:32', '2021-10-28 08:08:32'),
(56, 'affiliate_cookie_limit', NULL, '2021-10-28 08:08:32', '2021-10-28 08:08:32'),
(57, 'affiliate_payment', NULL, '2021-10-28 08:08:32', '2021-10-28 08:08:32');

-- --------------------------------------------------------

--
-- Table structure for table `thanas`
--

CREATE TABLE `thanas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `thana_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` float DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genders` enum('Male','Female','Other') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/avatar.png',
  `login_time` timestamp NULL DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(240) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_has_groups`
--

CREATE TABLE `user_has_groups` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_has_groups`
--

INSERT INTO `user_has_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 2),
(9, 2),
(13, 2),
(14, 5),
(15, 5),
(16, 5),
(17, 5),
(18, 5),
(19, 5),
(20, 5),
(21, 5),
(22, 5),
(23, 5),
(24, 5),
(25, 5),
(26, 5),
(27, 5),
(28, 5),
(29, 5),
(31, 5),
(32, 5),
(33, 5),
(34, 5),
(35, 5),
(30, 1),
(36, 2),
(37, 3),
(38, 3),
(39, 3),
(40, 2),
(41, 2),
(42, 2),
(43, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_has_permissions`
--

CREATE TABLE `user_has_permissions` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `variants`
--

CREATE TABLE `variants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variants`
--

INSERT INTO `variants` (`id`, `unit`, `variant`, `code`, `is_published`, `created_at`, `updated_at`) VALUES
(4, 'Size', 's', NULL, 1, '2020-08-26 23:11:45', '2020-08-26 23:11:45'),
(5, 'Size', 'm', NULL, 1, '2020-08-26 23:11:51', '2020-08-26 23:11:51'),
(6, 'Size', 'l', NULL, 1, '2020-08-26 23:11:56', '2020-08-26 23:11:56'),
(7, 'Size', 'xl', NULL, 1, '2020-08-26 23:12:04', '2020-08-26 23:12:04'),
(8, 'Size', 'xxl', NULL, 1, '2020-08-26 23:12:09', '2020-08-26 23:12:09'),
(11, 'Color', 'red', '#EF0000', 1, '2020-08-26 23:13:32', '2020-09-14 21:41:20'),
(12, 'Color', 'black', '#000000', 1, '2020-08-26 23:13:42', '2020-08-26 23:13:42'),
(13, 'Color', 'white', '#FFFFFF', 1, '2020-08-26 23:13:55', '2020-08-26 23:13:55'),
(14, 'Wheel', '2', NULL, 1, '2020-08-26 23:14:07', '2020-08-26 23:14:07'),
(15, 'Wheel', '4', NULL, 1, '2020-08-26 23:14:13', '2020-08-26 23:14:13'),
(16, 'Weight', '100 kg', NULL, 1, '2020-08-26 23:14:25', '2020-08-26 23:14:25'),
(17, 'Weight', '120 kg', NULL, 1, '2020-08-26 23:14:35', '2020-08-26 23:14:35'),
(18, 'Sleeve', 'full', NULL, 1, '2020-08-26 23:14:49', '2020-08-26 23:14:49'),
(19, 'Sleeve', 'half', NULL, 1, '2020-08-26 23:14:58', '2020-08-26 23:14:58'),
(20, 'Sleeve', 'sleeveless', NULL, 1, '2020-08-26 23:15:15', '2020-08-26 23:15:15'),
(24, 'Capacity', '4/64 gb', NULL, 1, '2020-08-26 23:17:13', '2020-08-26 23:17:13'),
(25, 'Capacity', '6/128 gb', NULL, 1, '2020-08-26 23:17:20', '2020-08-26 23:17:20'),
(26, 'Capacity', '500 gb', NULL, 1, '2020-08-26 23:17:34', '2020-08-26 23:17:34'),
(27, 'Capacity', '1 tb', NULL, 1, '2020-08-26 23:17:43', '2020-09-14 21:41:44'),
(28, 'Capacity', '12/264 gb', NULL, 1, '2020-08-26 23:18:17', '2020-08-26 23:18:17'),
(29, 'Size', '26', NULL, 1, '2020-09-13 15:28:49', '2020-09-13 15:28:49'),
(37, 'Color', 'pink', '#C631B8', 1, '2020-09-14 21:25:03', '2020-09-14 22:36:00'),
(38, 'Color', 'yellow', '#FD1414', 1, '2020-09-14 21:25:54', '2020-09-14 21:44:10'),
(39, 'Color', 'magenta', '#4B0808', 1, '2020-09-14 21:28:05', '2020-09-14 21:44:37'),
(40, 'Color', 'purple', '#A700FB', 0, '2020-09-24 08:02:16', '2020-09-24 08:02:16'),
(41, 'Color', 'red', '#F30404', 1, '2021-01-02 09:31:22', '2021-01-02 09:31:22');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` float DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_name` longtext COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trade_licence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `about` longtext COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_logo` longtext COLLATE utf8mb4_unicode_ci,
  `approve_status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` bigint(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_products`
--

CREATE TABLE `vendor_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_price` double NOT NULL,
  `purchase_price` double DEFAULT NULL,
  `is_discount` tinyint(1) DEFAULT NULL,
  `discount_type` enum('flat','per') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_price` double DEFAULT NULL,
  `discount_percentage` double DEFAULT NULL,
  `have_variant` tinyint(1) NOT NULL DEFAULT '0',
  `stock` int(11) DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_product_variant_stocks`
--

CREATE TABLE `vendor_product_variant_stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_product_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_variants_id` longtext COLLATE utf8mb4_unicode_ci,
  `product_variants` longtext COLLATE utf8mb4_unicode_ci,
  `quantity` int(11) NOT NULL,
  `extra_price` double NOT NULL DEFAULT '0',
  `alert_quantity` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_published` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addons`
--
ALTER TABLE `addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_commissions`
--
ALTER TABLE `admin_commissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `affiliate_accounts`
--
ALTER TABLE `affiliate_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `affiliate_accounts_user_id_unique` (`user_id`);

--
-- Indexes for table `affiliate_paid_histories`
--
ALTER TABLE `affiliate_paid_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `affiliate_payments_accounts`
--
ALTER TABLE `affiliate_payments_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `affiliate_payments_accounts_user_id_unique` (`user_id`);

--
-- Indexes for table `affiliate_product_links`
--
ALTER TABLE `affiliate_product_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `affiliate_sell_histories`
--
ALTER TABLE `affiliate_sell_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaign_products`
--
ALTER TABLE `campaign_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commissions`
--
ALTER TABLE `commissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complains`
--
ALTER TABLE `complains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliver_assigns`
--
ALTER TABLE `deliver_assigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliver_users`
--
ALTER TABLE `deliver_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliveymen_tracks`
--
ALTER TABLE `deliveymen_tracks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_campaign_products`
--
ALTER TABLE `ecom_campaign_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_carts`
--
ALTER TABLE `ecom_carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_deliver_assigns`
--
ALTER TABLE `ecom_deliver_assigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_deliver_tracks`
--
ALTER TABLE `ecom_deliver_tracks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_deliveymen_tracks`
--
ALTER TABLE `ecom_deliveymen_tracks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_orders`
--
ALTER TABLE `ecom_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_order_products`
--
ALTER TABLE `ecom_order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ecom_product_variant_stocks`
--
ALTER TABLE `ecom_product_variant_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infopages`
--
ALTER TABLE `infopages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logistics`
--
ALTER TABLE `logistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logistic_areas`
--
ALTER TABLE `logistic_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_has_permissions`
--
ALTER TABLE `module_has_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_contents`
--
ALTER TABLE `page_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_groups`
--
ALTER TABLE `page_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_variant_stocks`
--
ALTER TABLE `product_variant_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_views`
--
ALTER TABLE `product_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section_settings`
--
ALTER TABLE `section_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_accounts`
--
ALTER TABLE `seller_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_earnings`
--
ALTER TABLE `seller_earnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thanas`
--
ALTER TABLE `thanas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variants`
--
ALTER TABLE `variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_products`
--
ALTER TABLE `vendor_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_product_variant_stocks`
--
ALTER TABLE `vendor_product_variant_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addons`
--
ALTER TABLE `addons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_commissions`
--
ALTER TABLE `admin_commissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `affiliate_accounts`
--
ALTER TABLE `affiliate_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `affiliate_paid_histories`
--
ALTER TABLE `affiliate_paid_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `affiliate_payments_accounts`
--
ALTER TABLE `affiliate_payments_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `affiliate_product_links`
--
ALTER TABLE `affiliate_product_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `affiliate_sell_histories`
--
ALTER TABLE `affiliate_sell_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `campaign_products`
--
ALTER TABLE `campaign_products`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `commissions`
--
ALTER TABLE `commissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `complains`
--
ALTER TABLE `complains`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliver_assigns`
--
ALTER TABLE `deliver_assigns`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliver_users`
--
ALTER TABLE `deliver_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliveymen_tracks`
--
ALTER TABLE `deliveymen_tracks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ecom_campaign_products`
--
ALTER TABLE `ecom_campaign_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_carts`
--
ALTER TABLE `ecom_carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_deliver_assigns`
--
ALTER TABLE `ecom_deliver_assigns`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_deliver_tracks`
--
ALTER TABLE `ecom_deliver_tracks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_deliveymen_tracks`
--
ALTER TABLE `ecom_deliveymen_tracks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_orders`
--
ALTER TABLE `ecom_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_order_products`
--
ALTER TABLE `ecom_order_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ecom_product_variant_stocks`
--
ALTER TABLE `ecom_product_variant_stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `infopages`
--
ALTER TABLE `infopages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `logistics`
--
ALTER TABLE `logistics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logistic_areas`
--
ALTER TABLE `logistic_areas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `module_has_permissions`
--
ALTER TABLE `module_has_permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `page_contents`
--
ALTER TABLE `page_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `page_groups`
--
ALTER TABLE `page_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_variant_stocks`
--
ALTER TABLE `product_variant_stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_views`
--
ALTER TABLE `product_views`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `section_settings`
--
ALTER TABLE `section_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `seller_accounts`
--
ALTER TABLE `seller_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seller_earnings`
--
ALTER TABLE `seller_earnings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `thanas`
--
ALTER TABLE `thanas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `variants`
--
ALTER TABLE `variants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor_products`
--
ALTER TABLE `vendor_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor_product_variant_stocks`
--
ALTER TABLE `vendor_product_variant_stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
